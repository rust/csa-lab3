use std::fmt::{self, Debug};
use std::io::{self, ErrorKind, Read, Write};

use anyhow::{bail, Result};

pub enum IoPort {
    Reader(Box<dyn Read>),
    Writer(Box<dyn Write>),
}

impl IoPort {
    pub fn stdin() -> IoPort {
        IoPort::Reader(Box::new(io::stdin()))
    }

    pub fn stdout() -> IoPort {
        IoPort::Writer(Box::new(io::stdout()))
    }

    pub fn read(&mut self) -> Result<u32> {
        let IoPort::Reader(reader) = self else {
            bail!("attempt to read from a write port");
        };

        let mut buf = [0];
        let res = reader.read_exact(&mut buf);

        if res
            .as_ref()
            .is_err_and(|e| e.kind() == ErrorKind::UnexpectedEof)
        {
            return Ok(!0);
        }

        res?;

        Ok(buf[0] as u32)
    }

    pub fn write(&mut self, word: u32) -> Result<()> {
        let IoPort::Writer(writer) = self else {
            bail!("attempt to write to a read port");
        };

        let buf = [word as u8];
        writer.write_all(&buf)?;

        Ok(())
    }
}

impl Debug for IoPort {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("IoPort").finish_non_exhaustive()
    }
}
