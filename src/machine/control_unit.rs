use anyhow::{anyhow, Result};

use super::*;
use crate::isa::*;

#[derive(Debug, Default)]
pub struct ControlUnit {
    data_path: DataPath,
    state: State,
    log: Vec<String>,
}

#[derive(Debug, Clone, Copy, Default)]
struct State {
    halt: bool,
    exec_enable: bool,
    wb_enable: bool,
    wb_dst_reg: Reg,
    wb_src: WbSrc,
}

#[derive(Debug, Clone, Copy, Default)]
enum WbSrc {
    #[default]
    Mem,
    Res,
}

impl ControlUnit {
    pub fn new(initial_state: InitialState) -> ControlUnit {
        ControlUnit {
            data_path: DataPath::new(initial_state),
            state: State {
                halt: true,
                ..Default::default()
            },
            log: Vec::new(),
        }
    }

    pub fn log(&self) -> &[String] {
        &self.log
    }

    pub fn run(&mut self) -> Result<()> {
        while self.tick()? {}
        Ok(())
    }

    pub fn tick(&mut self) -> Result<bool> {
        // update to get instr
        self.data_path.update();

        // load rs1 as reg3 just in case we'll see a conditional instruction
        self.data_path.inputs.sel_rd_reg3 =
            Reg::decode(((self.data_path.outputs.instr >> 12) & 0xF) as u8).unwrap();
        // update to get zero flags
        self.data_path.update();

        let (next_state, flags) = control_logic(&mut self.log, self.state, self.data_path.outputs)?;

        self.data_path.inputs = flags;
        self.data_path.update();

        self.data_path.tick()?;
        self.state = next_state;

        self.log_tick();

        Ok(!self.state.halt)
    }

    fn log_tick(&mut self) {
        let [r0, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, ra, ip, sp] =
            self.data_path.regs();
        let res = self.data_path.res();
        let mem = self.data_path.outputs.instr;
        let _instr = Instr::decode(mem).filter(|_| self.state.exec_enable);

        self.log.extend([
            format!("R0  = {r0:08x}  R1  = {r1:08x}  R2  = {r2:08x}  R3  = {r3:08x}"),
            format!("R4  = {r4:08x}  R5  = {r5:08x}  R6  = {r6:08x}  R7  = {r7:08x}"),
            format!("R8  = {r8:08x}  R9  = {r9:08x}  R10 = {r10:08x}  R11 = {r11:08x}"),
            format!("R12 = {r12:08x}  RA  = {ra:08x}  IP  = {ip:08x}  SP  = {sp:08x}"),
            format!("Res = {res:08x}  Mem = {mem:08x}"),
            "".into(),
        ]);
    }
}

fn control_logic(
    log: &mut Vec<String>,
    state: State,
    inputs: DataPathOutputs,
) -> Result<(State, DataPathInputs)> {
    let mut next_state = State::default();
    let mut flags = DataPathInputs::default();

    let instr = if state.exec_enable {
        Instr::decode(inputs.instr)
            .ok_or_else(|| anyhow!("invalid instruction: {:08x}", inputs.instr))?
    } else {
        Instr::default()
    };

    let cond_is_zero = check_cond(
        state,
        instr,
        inputs.reg3_zero,
        inputs.mem_zero,
        inputs.res_zero,
    );

    let should_fetch = fetch_stall_logic(state, instr, cond_is_zero);
    if should_fetch {
        log.push("FETCH".into());
        fetch_logic(state, &mut next_state, &mut flags);
    }

    if state.exec_enable {
        log.push(format!("EXEC {instr}"));
        exec_logic(state, &mut next_state, &mut flags, instr, cond_is_zero);
    }

    if state.wb_enable {
        let src = match state.wb_src {
            WbSrc::Mem => "mem",
            WbSrc::Res => "res",
        };
        log.push(format!("WRITEBACK {src} -> {}", state.wb_dst_reg));
        wb_logic(state, &mut flags);
    }

    Ok((next_state, flags))
}

fn check_cond(state: State, instr: Instr, reg3_zero: bool, mem_zero: bool, res_zero: bool) -> bool {
    if state.wb_enable && state.wb_dst_reg == instr.rs1 {
        match state.wb_src {
            WbSrc::Mem => mem_zero,
            WbSrc::Res => res_zero,
        }
    } else {
        reg3_zero
    }
}

fn fetch_stall_logic(state: State, instr: Instr, cond_is_zero: bool) -> bool {
    if state.exec_enable {
        return match instr.opcode {
            Opcode::General(GeneralOp::Hlt | GeneralOp::Call) => false,
            Opcode::Mem(_) => false,
            Opcode::Cond(op) => op.is_zero != cond_is_zero || instr.rd != Reg::Ip,
            _ => instr.rd != Reg::Ip,
        };
    }

    true
}

fn fetch_logic(state: State, next_state: &mut State, flags: &mut DataPathInputs) {
    next_state.exec_enable = true;

    // load instr from memory
    flags.sel_mem_addr = SelMemAddr::Ip;
    flags.sel_mem_size = MemSize::Word;
    flags.do_rd_mem = true;

    // increment ip
    flags.sel_wr_ip_src = SelWrIpSrc::Ip;
    flags.do_wr_ip = true;

    if state.wb_enable && state.wb_dst_reg == Reg::Ip {
        // bypass from wb stage
        flags.sel_mem_addr = match state.wb_src {
            WbSrc::Mem => SelMemAddr::Mem,
            WbSrc::Res => SelMemAddr::Res,
        };
        flags.sel_wr_ip_src = match state.wb_src {
            WbSrc::Mem => SelWrIpSrc::Mem,
            WbSrc::Res => SelWrIpSrc::Res,
        };
        flags.do_wr_ip = true;
    }
}

fn exec_logic(
    state: State,
    next_state: &mut State,
    flags: &mut DataPathInputs,
    instr: Instr,
    cond_is_zero: bool,
) {
    flags.sel_cmp_op = CmpOp::Eq;
    flags.sel_res = SelRes::Alu;
    flags.sel_rd_reg1 = instr.rs1;
    flags.sel_rd_reg2 = instr.rs2;

    next_state.wb_enable = true;
    next_state.wb_src = WbSrc::Res;
    next_state.wb_dst_reg = instr.rd;

    match instr.opcode {
        Opcode::General(op) => {
            flags.sel_alu_op = AluOp::Xor;
            flags.sel_alu_in1 = SelAluIn1::Zero;
            flags.sel_alu_in2 = if instr.has_imm {
                SelAluIn2::Imm
            } else {
                SelAluIn2::Reg2
            };

            match op {
                GeneralOp::Nop => next_state.wb_enable = false,
                GeneralOp::Hlt => next_state.halt = true,
                GeneralOp::Mov => (),
                GeneralOp::Not => flags.sel_alu_in1 = SelAluIn1::Ones,
                GeneralOp::Neg => {
                    flags.sel_alu_op = AluOp::Sub;
                }
                GeneralOp::Call => {
                    if instr.has_imm {
                        flags.sel_alu_op = AluOp::Add;
                        flags.sel_alu_in1 = SelAluIn1::Reg1;
                        flags.sel_rd_reg1 = Reg::Ip;
                    }

                    flags.do_wr_ra = true;
                    next_state.wb_dst_reg = Reg::Ip;
                }
            }
        }

        Opcode::Mem(MemOp { is_store, size }) => {
            flags.sel_alu_op = AluOp::Add;
            flags.sel_alu_in1 = SelAluIn1::Reg1;
            flags.sel_alu_in2 = if instr.has_imm {
                SelAluIn2::Imm
            } else {
                SelAluIn2::Reg2
            };

            flags.sel_mem_addr = SelMemAddr::AluRes;
            flags.sel_mem_size = size;
            flags.do_rd_mem = !is_store;
            flags.do_wr_mem = is_store;

            if is_store {
                next_state.wb_enable = false;
                flags.sel_rd_reg3 = instr.rd;
            } else {
                next_state.wb_src = WbSrc::Mem;
            }
        }

        Opcode::Alu(op) => {
            flags.sel_alu_op = op;
            flags.sel_alu_in1 = SelAluIn1::Reg1;
            flags.sel_alu_in2 = if instr.has_imm {
                SelAluIn2::Imm
            } else {
                SelAluIn2::Reg2
            };
        }

        Opcode::Cmp(op) => {
            flags.sel_cmp_op = op;
            flags.sel_res = SelRes::Cmp;
            flags.sel_alu_op = AluOp::Sub;
            flags.sel_alu_in1 = SelAluIn1::Reg1;
            flags.sel_alu_in2 = if instr.has_imm {
                SelAluIn2::Imm
            } else {
                SelAluIn2::Reg2
            };
        }

        Opcode::Cond(op) => {
            flags.sel_alu_op = AluOp::Add;
            flags.sel_alu_in1 = match op.kind {
                CondOpKind::Mov => SelAluIn1::Zero,
                CondOpKind::Adda => {
                    flags.sel_rd_reg1 = instr.rd;
                    SelAluIn1::Reg1
                }
            };

            flags.sel_alu_in2 = if instr.has_imm {
                SelAluIn2::Imm
            } else {
                SelAluIn2::Reg2
            };

            flags.sel_rd_reg3 = instr.rs1;
            next_state.wb_enable = op.is_zero == cond_is_zero;
        }

        Opcode::Io(IoOp::In) => {
            flags.sel_res = SelRes::Io;
            flags.sel_io_mode = SelIoMode::In;
        }

        Opcode::Io(IoOp::Out) => {
            flags.sel_alu_op = AluOp::Or;
            flags.sel_alu_in1 = SelAluIn1::Reg1;
            flags.sel_alu_in2 = SelAluIn2::Zero;
            flags.sel_io_mode = SelIoMode::Out;
            next_state.wb_enable = false;
        }
    }

    if state.wb_enable {
        // bypass from wb stage

        if flags.sel_alu_in1 == SelAluIn1::Reg1 && flags.sel_rd_reg1 == state.wb_dst_reg {
            flags.sel_alu_in1 = match state.wb_src {
                WbSrc::Mem => SelAluIn1::Mem,
                WbSrc::Res => SelAluIn1::Res,
            };
        }

        if flags.sel_alu_in2 == SelAluIn2::Reg2 && flags.sel_rd_reg2 == state.wb_dst_reg {
            flags.sel_alu_in2 = match state.wb_src {
                WbSrc::Mem => SelAluIn2::Mem,
                WbSrc::Res => SelAluIn2::Res,
            };
        }

        if flags.do_wr_mem
            && flags.sel_mem_wr_data == SelMemWrData::Reg3
            && flags.sel_rd_reg3 == state.wb_dst_reg
        {
            flags.sel_mem_wr_data = match state.wb_src {
                WbSrc::Mem => SelMemWrData::Mem,
                WbSrc::Res => SelMemWrData::Res,
            };
        }
    }
}

fn wb_logic(state: State, flags: &mut DataPathInputs) {
    if state.wb_dst_reg == Reg::Ip {
        return;
    }

    flags.sel_wr_reg_src = match state.wb_src {
        WbSrc::Mem => SelWrRegSrc::Mem,
        WbSrc::Res => SelWrRegSrc::Res,
    };
    flags.sel_wr_reg = state.wb_dst_reg;
    flags.do_wr_reg = true;
}
