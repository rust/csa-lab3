use crate::isa::AluOp;

#[derive(Debug, Default)]
pub struct Alu {
    pub inputs: AluInputs,
    pub outputs: AluOutputs,
}

#[derive(Debug, Clone, Copy, Default)]
pub struct AluInputs {
    pub sel_op: AluOp,
    pub in1: u32,
    pub in2: u32,
}

#[derive(Debug, Clone, Copy, Default)]
pub struct AluOutputs {
    pub res: u32,
    pub flags: AluFlags,
}

#[derive(Debug, Clone, Copy, Default)]
pub struct AluFlags {
    pub zero: bool,
    pub carry: bool,
    pub overflow: bool,
    pub sign: bool,
}

impl Alu {
    pub fn new() -> Alu {
        Alu::default()
    }

    pub fn update(&mut self) {
        let a = self.inputs.in1;
        let b = self.inputs.in2;
        let a_s = a as i32;
        let b_s = b as i32;

        self.outputs.flags.carry = false;
        self.outputs.flags.overflow = false;

        self.outputs.res = match self.inputs.sel_op {
            AluOp::Add => {
                self.outputs.flags.carry = a.checked_add(b).is_none();
                self.outputs.flags.overflow = a_s.checked_add(b_s).is_none();
                a.wrapping_add(b)
            }
            AluOp::Sub => {
                self.outputs.flags.carry = a.checked_sub(b).is_none();
                self.outputs.flags.overflow = a_s.checked_sub(b_s).is_none();
                a.wrapping_sub(b)
            }
            AluOp::And => a & b,
            AluOp::Or => a | b,
            AluOp::Xor => a ^ b,
            AluOp::Shl => a << b,
            AluOp::Shrl => a >> b,
            AluOp::Shra => (a_s >> b) as u32,
            AluOp::Muls => a_s.wrapping_mul(b_s) as u32,
            AluOp::Divs if b_s != 0 => a_s.wrapping_div(b_s) as u32,
            AluOp::Divs => 0,
            AluOp::Rems if b_s != 0 => a_s.wrapping_rem(b_s) as u32,
            AluOp::Rems => 0,
            AluOp::Mulu => a.wrapping_mul(b),
            AluOp::Divu if b != 0 => a.wrapping_div(b),
            AluOp::Divu => 0,
            AluOp::Remu if b != 0 => a.wrapping_rem(b),
            AluOp::Remu => 0,
        };

        self.outputs.flags.zero = self.outputs.res == 0;
        self.outputs.flags.sign = self.outputs.res >> 31 == 1;
    }
}
