use anyhow::{anyhow, bail, Result};

use super::{Alu, Comparator, IoPort, Memory, RegFile};
use crate::isa::{AluOp, CmpOp, MemSize, Reg};

#[derive(Debug, Default)]
pub struct DataPath {
    pub inputs: DataPathInputs,
    pub outputs: DataPathOutputs,
    reg_file: RegFile,
    memory: Memory,
    alu: Alu,
    comparator: Comparator,
    io_ports: Vec<IoPort>,
    res: u32,
}

#[derive(Debug)]
pub struct InitialState {
    pub memory: Vec<u8>,
    pub ip: u32,
    pub sp: u32,
    pub io_ports: Vec<IoPort>,
}

#[derive(Debug, Clone, Copy, Default)]
pub struct DataPathInputs {
    pub sel_mem_addr: SelMemAddr,
    pub sel_mem_wr_data: SelMemWrData,
    pub sel_mem_size: MemSize,
    pub do_rd_mem: bool,
    pub do_wr_mem: bool,

    pub sel_alu_in1: SelAluIn1,
    pub sel_alu_in2: SelAluIn2,
    pub sel_alu_op: AluOp,

    pub sel_cmp_op: CmpOp,
    pub sel_res: SelRes,

    pub sel_wr_ip_src: SelWrIpSrc,
    pub sel_wr_reg_src: SelWrRegSrc,
    pub sel_rd_reg1: Reg,
    pub sel_rd_reg2: Reg,
    pub sel_rd_reg3: Reg,
    pub sel_wr_reg: Reg,
    pub do_wr_ip: bool,
    pub do_wr_ra: bool,
    pub do_wr_reg: bool,

    pub sel_io_mode: SelIoMode,
}

#[derive(Debug, Clone, Copy, Default)]
pub struct DataPathOutputs {
    pub instr: u32,
    pub reg3_zero: bool,
    pub mem_zero: bool,
    pub res_zero: bool,
}

impl DataPath {
    pub fn new(initial_state: InitialState) -> DataPath {
        DataPath {
            inputs: DataPathInputs::default(),
            outputs: DataPathOutputs::default(),
            reg_file: RegFile::new()
                .with_ip(initial_state.ip)
                .with_sp(initial_state.sp),
            memory: Memory::new(initial_state.memory),
            alu: Alu::default(),
            comparator: Comparator::default(),
            io_ports: initial_state.io_ports,
            res: 0,
        }
    }

    pub fn regs(&self) -> &[u32; 16] {
        self.reg_file.regs()
    }

    pub fn res(&self) -> u32 {
        self.res
    }

    pub fn update(&mut self) {
        self.reg_file.update();

        self.reg_file.inputs.sel_rd_reg1 = self.inputs.sel_rd_reg1;
        self.reg_file.inputs.sel_rd_reg2 = self.inputs.sel_rd_reg2;
        self.reg_file.inputs.sel_rd_reg3 = self.inputs.sel_rd_reg3;
        self.reg_file.inputs.sel_wr_reg = self.inputs.sel_wr_reg;
        self.reg_file.inputs.do_wr = self.inputs.do_wr_reg;
        self.reg_file.inputs.do_wr_ip = self.inputs.do_wr_ip;
        self.reg_file.inputs.do_wr_ra = self.inputs.do_wr_ra;
        self.reg_file.inputs.wr_data = match self.inputs.sel_wr_reg_src {
            SelWrRegSrc::Mem => self.memory.output,
            SelWrRegSrc::Res => self.res,
        };
        self.reg_file.inputs.wr_ip_data = {
            let val = match self.inputs.sel_wr_ip_src {
                SelWrIpSrc::Mem => self.memory.output,
                SelWrIpSrc::Res => self.res,
                SelWrIpSrc::Ip => self.reg_file.outputs.rd_ip,
            };
            val.wrapping_add(4)
        };
        self.reg_file.inputs.wr_ra_data = self.reg_file.outputs.rd_ip;

        self.reg_file.update();

        self.alu.inputs.sel_op = self.inputs.sel_alu_op;
        self.alu.inputs.in1 = match self.inputs.sel_alu_in1 {
            SelAluIn1::Reg1 => self.reg_file.outputs.rd_reg1,
            SelAluIn1::Res => self.res,
            SelAluIn1::Mem => self.memory.output,
            SelAluIn1::Zero => 0,
            SelAluIn1::Ones => !0,
        };
        self.alu.inputs.in2 = match self.inputs.sel_alu_in2 {
            SelAluIn2::Reg2 => self.reg_file.outputs.rd_reg2,
            SelAluIn2::Res => self.res,
            SelAluIn2::Mem => self.memory.output,
            SelAluIn2::Imm => (self.memory.output >> 16) as i16 as i32 as u32, // sign extend
            SelAluIn2::Zero => 0,
        };

        self.alu.update();

        self.comparator.inputs.sel_op = self.inputs.sel_cmp_op;
        self.comparator.inputs.flags = self.alu.outputs.flags;

        self.comparator.update();

        self.memory.inputs.sel_size = self.inputs.sel_mem_size;
        self.memory.inputs.do_wr = self.inputs.do_wr_mem;
        self.memory.inputs.do_rd = self.inputs.do_rd_mem;
        self.memory.inputs.wr_data = match self.inputs.sel_mem_wr_data {
            SelMemWrData::Reg3 => self.reg_file.outputs.rd_reg3,
            SelMemWrData::Res => self.res,
            SelMemWrData::Mem => self.memory.output,
        };
        self.memory.inputs.addr = match self.inputs.sel_mem_addr {
            SelMemAddr::Ip => self.reg_file.outputs.rd_ip,
            SelMemAddr::Res => self.res,
            SelMemAddr::AluRes => self.alu.outputs.res,
            SelMemAddr::Mem => self.memory.output,
        };

        self.outputs.instr = self.memory.output;
        self.outputs.reg3_zero = self.reg_file.outputs.rd_reg3 == 0;
        self.outputs.mem_zero = self.memory.output == 0;
        self.outputs.res_zero = self.res == 0;
    }

    pub fn tick(&mut self) -> Result<()> {
        self.res = match self.inputs.sel_res {
            SelRes::Alu => self.alu.outputs.res,
            SelRes::Cmp => self.comparator.output as u32,
            SelRes::Io => {
                if self.inputs.sel_io_mode != SelIoMode::In {
                    bail!("sel_res set to io while not reading from io");
                }

                let idx = (self.memory.output >> 16) as usize;
                self.io_ports
                    .get_mut(idx)
                    .ok_or_else(|| anyhow!("no such io port: {idx}"))?
                    .read()?
            }
        };

        if self.inputs.sel_io_mode == SelIoMode::Out {
            let idx = (self.memory.output >> 16) as usize;
            self.io_ports
                .get_mut(idx)
                .ok_or_else(|| anyhow!("no such io port: {idx}"))?
                .write(self.alu.outputs.res)?;
        }

        self.reg_file.tick()?;
        self.memory.tick()?;

        Ok(())
    }
}

#[derive(Debug, Clone, Copy, Default, Eq, PartialEq)]
pub enum SelWrIpSrc {
    #[default]
    Mem,
    Res,
    Ip,
}

#[derive(Debug, Clone, Copy, Default, Eq, PartialEq)]
pub enum SelWrRegSrc {
    #[default]
    Mem,
    Res,
}

#[derive(Debug, Clone, Copy, Default, Eq, PartialEq)]
pub enum SelMemAddr {
    #[default]
    Ip,
    Res,
    AluRes,
    Mem,
}

#[derive(Debug, Clone, Copy, Default, Eq, PartialEq)]
pub enum SelMemWrData {
    #[default]
    Reg3,
    Res,
    Mem,
}

#[derive(Debug, Clone, Copy, Default, Eq, PartialEq)]
pub enum SelAluIn1 {
    #[default]
    Reg1,
    Res,
    Mem,
    Zero,
    Ones,
}

#[derive(Debug, Clone, Copy, Default, Eq, PartialEq)]
pub enum SelAluIn2 {
    #[default]
    Reg2,
    Res,
    Mem,
    Imm,
    Zero,
}

#[derive(Debug, Clone, Copy, Default, Eq, PartialEq)]
pub enum SelRes {
    #[default]
    Alu,
    Cmp,
    Io,
}

#[derive(Debug, Clone, Copy, Default, Eq, PartialEq)]
pub enum SelIoMode {
    #[default]
    Off,
    In,
    Out,
}
