use super::AluFlags;
use crate::isa::CmpOp;

#[derive(Debug, Default)]
pub struct Comparator {
    pub inputs: ComparatorInputs,
    pub output: bool,
}

#[derive(Debug, Clone, Copy, Default)]
pub struct ComparatorInputs {
    pub sel_op: CmpOp,
    pub flags: AluFlags,
}

impl Comparator {
    pub fn new() -> Comparator {
        Comparator::default()
    }

    pub fn update(&mut self) {
        let AluFlags {
            zero,
            carry,
            overflow,
            sign,
        } = self.inputs.flags;

        self.output = match self.inputs.sel_op {
            CmpOp::Eq => zero,
            CmpOp::Ne => !zero,
            CmpOp::Gts => !zero && sign == overflow,
            CmpOp::Lts => sign != overflow,
            CmpOp::Ges => sign == overflow,
            CmpOp::Les => zero || sign != overflow,
            CmpOp::Gtu => !carry && !zero,
            CmpOp::Ltu => carry,
            CmpOp::Geu => !carry,
            CmpOp::Leu => carry || zero,
        };
    }
}
