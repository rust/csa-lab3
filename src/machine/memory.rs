use anyhow::{anyhow, bail, Result};

use crate::isa::MemSize;

#[derive(Debug, Default)]
pub struct Memory {
    pub inputs: MemoryInputs,
    pub output: u32,
    data: Vec<u8>,
}

#[derive(Debug, Default)]
pub struct MemoryInputs {
    pub sel_size: MemSize,
    pub do_wr: bool,
    pub do_rd: bool,
    pub wr_data: u32,
    pub addr: u32,
}

impl Memory {
    pub fn new(data: Vec<u8>) -> Memory {
        Memory {
            inputs: MemoryInputs::default(),
            output: 0,
            data,
        }
    }

    pub fn tick(&mut self) -> Result<()> {
        if self.inputs.do_rd && self.inputs.do_wr {
            bail!("attempt to read and write to memory concurrently");
        }

        let addr = self.inputs.addr as usize;

        if self.inputs.do_rd {
            self.output = match self.inputs.sel_size {
                MemSize::Byte => {
                    let b = self
                        .data
                        .get(addr)
                        .ok_or_else(|| anyhow!("memory access out of bounds ({addr})"))?;
                    (*b as i8) as i32 as u32 // sign extend
                }
                MemSize::Half => {
                    let b = self
                        .data
                        .get(addr..addr + 2)
                        .ok_or_else(|| anyhow!("memory access out of bounds ({addr})"))?;
                    i16::from_le_bytes([b[0], b[1]]) as i32 as u32 // sign extend
                }
                MemSize::Word => {
                    let b = self
                        .data
                        .get(addr..addr + 4)
                        .ok_or_else(|| anyhow!("memory access out of bounds ({addr})"))?;
                    u32::from_le_bytes([b[0], b[1], b[2], b[3]])
                }
            };
        }

        if self.inputs.do_wr {
            match self.inputs.sel_size {
                MemSize::Byte => {
                    let b = self
                        .data
                        .get_mut(addr)
                        .ok_or_else(|| anyhow!("memory access out of bounds ({addr})"))?;
                    *b = self.inputs.wr_data as u8; // truncate
                }
                MemSize::Half => {
                    let b = self
                        .data
                        .get_mut(addr..addr + 2)
                        .ok_or_else(|| anyhow!("memory access out of bounds ({addr})"))?;
                    b[0] = self.inputs.wr_data as u8;
                    b[1] = (self.inputs.wr_data >> 8) as u8;
                }
                MemSize::Word => {
                    let b = self
                        .data
                        .get_mut(addr..addr + 4)
                        .ok_or_else(|| anyhow!("memory access out of bounds ({addr})"))?;
                    b[0] = self.inputs.wr_data as u8;
                    b[1] = (self.inputs.wr_data >> 8) as u8;
                    b[2] = (self.inputs.wr_data >> 16) as u8;
                    b[3] = (self.inputs.wr_data >> 24) as u8;
                }
            }
        }

        Ok(())
    }
}
