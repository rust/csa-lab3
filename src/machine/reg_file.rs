use anyhow::{bail, Result};

use crate::isa::Reg;

#[derive(Debug, Default)]
pub struct RegFile {
    pub inputs: RegFileInputs,
    pub outputs: RegFileOutputs,
    regs: [u32; 16],
}

#[derive(Debug, Clone, Copy, Default)]
pub struct RegFileInputs {
    pub sel_rd_reg1: Reg,
    pub sel_rd_reg2: Reg,
    pub sel_rd_reg3: Reg,
    pub sel_wr_reg: Reg,
    pub do_wr: bool,
    pub do_wr_ip: bool,
    pub do_wr_ra: bool,
    pub wr_data: u32,
    pub wr_ip_data: u32,
    pub wr_ra_data: u32,
}

#[derive(Debug, Clone, Copy, Default)]
pub struct RegFileOutputs {
    pub rd_ip: u32,
    pub rd_reg1: u32,
    pub rd_reg2: u32,
    pub rd_reg3: u32,
}

impl RegFile {
    pub fn new() -> RegFile {
        RegFile {
            inputs: RegFileInputs::default(),
            outputs: RegFileOutputs::default(),
            regs: [0; 16],
        }
    }

    pub fn with_ip(mut self, val: u32) -> Self {
        self.regs[Reg::Ip as usize] = val;
        self
    }

    pub fn with_sp(mut self, val: u32) -> Self {
        self.regs[Reg::Sp as usize] = val;
        self
    }

    pub fn regs(&self) -> &[u32; 16] {
        &self.regs
    }

    pub fn update(&mut self) {
        self.outputs.rd_ip = self.regs[Reg::Ip as usize];
        self.outputs.rd_reg1 = self.regs[self.inputs.sel_rd_reg1 as usize];
        self.outputs.rd_reg2 = self.regs[self.inputs.sel_rd_reg2 as usize];
        self.outputs.rd_reg3 = self.regs[self.inputs.sel_rd_reg3 as usize];
    }

    pub fn tick(&mut self) -> Result<()> {
        if self.inputs.do_wr_ip {
            if self.inputs.do_wr && self.inputs.sel_wr_reg == Reg::Ip {
                bail!("attempt to write to ip concurrently");
            }

            self.regs[Reg::Ip as usize] = self.inputs.wr_ip_data;
        }

        if self.inputs.do_wr_ra {
            if self.inputs.do_wr && self.inputs.sel_wr_reg == Reg::Ra {
                bail!("attempt to write to ra concurrently");
            }

            self.regs[Reg::Ra as usize] = self.inputs.wr_ra_data;
        }

        if self.inputs.do_wr {
            self.regs[self.inputs.sel_wr_reg as usize] = self.inputs.wr_data;
        }

        Ok(())
    }
}
