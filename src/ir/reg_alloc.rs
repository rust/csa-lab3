use std::hash::BuildHasherDefault;

use ahash::AHasher;
use indexmap::{IndexMap, IndexSet};
use priority_queue::PriorityQueue;

use super::*;
use crate::isa::*;

type Vertex = (BasicBlockId, usize);
type Successors = IndexMap<Vertex, Vec<Vertex>>;
type Sets = IndexMap<Vertex, IndexSet<VReg>>;
type NumReferences = IndexMap<VReg, usize>;
type Colors = IndexMap<VReg, Reg>;
type SameColorHeuristic = IndexMap<VReg, IndexSet<VReg>>;

pub fn reg_alloc(ir: &mut IrRoot) {
    for func in &mut ir.funcs {
        func_reg_alloc(func);
    }
}

pub fn func_reg_alloc(func: &mut Func) {
    loop {
        let liveness = liveness_analysis(func);
        let graph = build_interference_graph(&liveness);
        let mut colors = get_initial_colors(func);
        let same_color = get_same_color_heuristic(func);
        let colorable =
            color_interference_graph(&graph, &mut colors, Reg::GENERAL_PURPOSE, &same_color);
        if colorable {
            apply_colors(func, &colors);
            break;
        } else {
            let vreg = select_spill_vreg(&graph, &colors, &liveness.num_references);
            spill(func, vreg);
        }
    }
}

struct LivenessInfo {
    in_sets: Sets,
    out_sets: Sets,
    num_references: NumReferences,
}

fn liveness_analysis(func: &mut Func) -> LivenessInfo {
    let mut vertices = Vec::new();
    let mut successors = Successors::default();
    let mut use_sets = Sets::default();
    let mut def_sets = Sets::default();
    let mut in_sets = Sets::default();
    let mut out_sets = Sets::default();
    let mut num_references = NumReferences::default();

    for (bb_id, bb) in func.basic_blocks.iter().enumerate() {
        let bb_id = BasicBlockId(bb_id);

        for (i, instr) in bb.instrs.iter().enumerate() {
            let vertex = (bb_id, i);
            vertices.push(vertex);

            let use_set = use_sets.entry(vertex).or_default();
            let def_set = def_sets.entry(vertex).or_default();

            for vreg in instr.uses() {
                use_set.insert(vreg);
                *num_references.entry(vreg).or_insert(0) += 1;
            }

            for vreg in instr.defs() {
                def_set.insert(vreg);
                *num_references.entry(vreg).or_insert(0) += 1;
            }

            let succ_list = successors.entry(vertex).or_default();
            succ_list.push((bb_id, i + 1));
        }

        let vertex = (bb_id, bb.instrs.len());
        vertices.push(vertex);

        let use_set = use_sets.entry(vertex).or_default();
        let def_set = def_sets.entry(vertex).or_default();

        use_set.extend(bb.terminator.uses());
        def_set.clear();

        for bb_id in bb.successors() {
            let succ_list = successors.entry(vertex).or_default();
            succ_list.push((bb_id, 0));
        }
    }

    loop {
        let old_in_sets = in_sets.clone();
        let old_out_sets = out_sets.clone();

        for &vertex in &vertices {
            let use_set = use_sets.entry(vertex).or_default();
            let def_set = def_sets.entry(vertex).or_default();
            let in_set = in_sets.entry(vertex).or_default();
            let out_set = out_sets.entry(vertex).or_default();
            let succ_list = successors.entry(vertex).or_default();

            in_set.clear();

            for &v in use_set.iter() {
                in_set.insert(v);
            }

            for &v in out_set.iter() {
                if !def_set.contains(&v) {
                    in_set.insert(v);
                }
            }

            out_set.clear();

            for &succ in succ_list.iter() {
                let in_set = in_sets.entry(succ).or_default();
                for v in in_set.iter() {
                    out_set.insert(*v);
                }
            }
        }

        if in_sets == old_in_sets && out_sets == old_out_sets {
            break;
        }
    }

    LivenessInfo {
        in_sets,
        out_sets,
        num_references,
    }
}

type InterferenceGraph = IndexMap<VReg, IndexSet<VReg>>;

fn build_interference_graph(liveness: &LivenessInfo) -> InterferenceGraph {
    let mut graph = InterferenceGraph::default();

    for (&vreg, _) in &liveness.num_references {
        graph.insert(vreg, Default::default());
    }

    for set in liveness.in_sets.values().chain(liveness.out_sets.values()) {
        for &a in set {
            for &b in set {
                if a != b {
                    graph.entry(a).or_default().insert(b);
                    graph.entry(b).or_default().insert(a);
                }
            }
        }
    }

    graph
}

fn get_initial_colors(func: &Func) -> Colors {
    let mut colors = Colors::default();

    for (vreg, info) in func.vregs.iter().enumerate() {
        let vreg = VReg(vreg);
        if let Some(reg) = info.reg {
            colors.insert(vreg, reg);
        }
    }

    colors
}

fn apply_colors(func: &mut Func, colors: &Colors) {
    for (&vreg, &reg) in colors {
        func.vregs[vreg.0].reg = Some(reg);
    }
}

fn get_same_color_heuristic(func: &Func) -> SameColorHeuristic {
    let mut set = SameColorHeuristic::default();

    for bb in &func.basic_blocks {
        for instr in &bb.instrs {
            if let IrInstr::Copy { src, dst } = *instr {
                set.entry(src).or_default().insert(dst);
                set.entry(dst).or_default().insert(src);
            }
        }
    }

    set
}

fn color_interference_graph(
    graph: &InterferenceGraph,
    colors: &mut Colors,
    allowed_colors: &[Reg],
    same_color_heuristic: &SameColorHeuristic,
) -> bool {
    let mut pq = PriorityQueue::<_, _, BuildHasherDefault<AHasher>>::default();

    for (&node, neighbors) in graph {
        pq.push(node, 999999 - neighbors.len());
    }

    let mut colorable = true;

    'outer: while let Some((node, _)) = pq.pop() {
        if colors.contains_key(&node) {
            continue;
        }

        let mut used = IndexSet::new();

        for neighbor in graph.get(&node).unwrap() {
            if let Some(&color) = colors.get(neighbor) {
                used.insert(color);
            } else {
                pq.change_priority_by(neighbor, |p| *p += 1);
            }
        }

        if let Some(list) = same_color_heuristic.get(&node) {
            for vreg in list {
                if let Some(color) = colors.get(vreg) {
                    if !used.contains(color) {
                        colors.insert(node, *color);
                        continue 'outer;
                    }
                }
            }
        }

        let Some(color) = allowed_colors.iter().copied().find(|v| !used.contains(v)) else {
            colorable = false;
            continue;
        };

        colors.insert(node, color);
    }

    colorable
}

fn select_spill_vreg(
    graph: &InterferenceGraph,
    colors: &Colors,
    num_references: &NumReferences,
) -> VReg {
    num_references
        .iter()
        .filter(|(&vreg, _)| !colors.contains_key(&vreg) && graph.contains_key(&vreg))
        .min_by_key(|(&vreg, &num_refs)| {
            let degree = graph[&vreg].len();
            num_refs * 10000 / degree
        })
        .map(|(&vreg, _)| vreg)
        .unwrap()
}

fn spill(func: &mut Func, vreg: VReg) {
    let slot = func.used_slots;
    func.used_slots += 1;

    for bb in &mut func.basic_blocks {
        let mut idx = 0;
        while idx < bb.instrs.len() {
            let instr = &bb.instrs[idx];

            if instr.defs().any(|v| v == vreg) {
                bb.instrs
                    .insert(idx + 1, IrInstr::StoreVar { src: vreg, slot });
                idx += 1;
            } else if instr.uses().any(|v| v == vreg) {
                let new_name = VReg(func.vregs.len());
                func.vregs.push(func.vregs[vreg.0].clone());

                bb.instrs[idx].rename_vreg(vreg, new_name);

                bb.instrs.insert(
                    idx,
                    IrInstr::LoadVar {
                        dst: new_name,
                        slot,
                    },
                );

                idx += 1;
            }

            idx += 1;
        }
    }
}
