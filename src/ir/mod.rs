mod lowering;
mod passes;
mod reg_alloc;

use std::collections::{HashMap, HashSet};
use std::fmt::{self, Display};

use anyhow::{bail, Error, Result};
use arrayvec::ArrayVec;

pub use self::lowering::lower;
pub use self::passes::simplify;
pub use self::reg_alloc::reg_alloc;
use crate::isa::Reg;
use crate::syntax::*;

#[derive(Debug, Clone, Copy, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct VReg(pub usize);

impl Display for VReg {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "v{}", self.0)
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash, Default)]
pub struct ConstId(pub usize);

impl Display for ConstId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "c{}", self.0)
    }
}

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub enum Const {
    Bool(bool),
    I32(i32),
    String(String),
}

#[derive(Debug, Default)]
pub struct Consts {
    by_id: Vec<Const>,
    by_value: HashMap<Const, ConstId>,
}

impl Consts {
    pub fn get(&self, id: ConstId) -> &Const {
        &self.by_id[id.0]
    }

    pub fn get_or_insert(&mut self, value: Const) -> ConstId {
        if let Some(id) = self.by_value.get(&value) {
            return *id;
        }

        let id = ConstId(self.by_id.len());

        self.by_id.push(value.clone());
        self.by_value.insert(value.clone(), id);

        id
    }

    pub fn iter(&self) -> impl Iterator<Item = (ConstId, &Const)> {
        self.by_id.iter().enumerate().map(|(i, v)| (ConstId(i), v))
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash)]
pub enum Type {
    Never,
    Unit,
    Bool,
    I32,
    Str,
}

impl Type {
    pub fn parse(expr: &Expr) -> Result<Self> {
        Ok(match &expr.kind {
            ExprKind::Symbol(sym) => match sym.as_str() {
                "!" => Type::Never,
                "bool" => Type::Bool,
                "i32" => Type::I32,
                "str" => Type::Str,
                _ => bail!("{}: unknown type `{sym}`", expr.range),
            },
            ExprKind::List(list) if list.is_empty() => Type::Unit,
            _ => bail!("{}: type expected", expr.range),
        })
    }

    pub fn is(self, ty: Type) -> bool {
        if self == Type::Never || ty == Type::Never {
            return true;
        }

        self == ty
    }
}

impl Display for Type {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(match self {
            Type::Never => "!",
            Type::Unit => "()",
            Type::Bool => "bool",
            Type::I32 => "i32",
            Type::Str => "str",
        })
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash)]
pub enum UnOp {
    Not,
    Neg,
}

impl TryFrom<&str> for UnOp {
    type Error = Error;

    fn try_from(value: &str) -> Result<Self> {
        Ok(match value {
            "!" => UnOp::Neg,
            "-" => UnOp::Neg,
            _ => bail!("unknown unary operator `{value}`"),
        })
    }
}

impl Display for UnOp {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(match self {
            UnOp::Not => "!",
            UnOp::Neg => "-",
        })
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash)]
pub enum BinOp {
    Add,
    Sub,
    And,
    Or,
    Xor,
    Shl,
    Shr,
    Mul,
    Div,
    Rem,
    CmpEq,
    CmpNe,
    CmpGt,
    CmpLt,
    CmpGe,
    CmpLe,
}

impl TryFrom<&str> for BinOp {
    type Error = Error;

    fn try_from(value: &str) -> Result<Self> {
        Ok(match value {
            "+" => BinOp::Add,
            "-" => BinOp::Sub,
            "&" => BinOp::And,
            "|" => BinOp::Or,
            "^" => BinOp::Xor,
            "<<" => BinOp::Shl,
            ">>" => BinOp::Shr,
            "*" => BinOp::Mul,
            "/" => BinOp::Div,
            "%" => BinOp::Rem,
            "==" => BinOp::CmpEq,
            "!=" => BinOp::CmpNe,
            ">" => BinOp::CmpGt,
            "<" => BinOp::CmpLt,
            ">=" => BinOp::CmpGe,
            "<=" => BinOp::CmpLe,
            _ => bail!("unknown binary operator `{value}`"),
        })
    }
}

impl Display for BinOp {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(match self {
            BinOp::Add => "+",
            BinOp::Sub => "-",
            BinOp::And => "&",
            BinOp::Or => "|",
            BinOp::Xor => "^",
            BinOp::Shl => "<<",
            BinOp::Shr => ">>",
            BinOp::Mul => "*",
            BinOp::Div => "/",
            BinOp::Rem => "%",
            BinOp::CmpEq => "==",
            BinOp::CmpNe => "!=",
            BinOp::CmpGt => ">",
            BinOp::CmpLt => "<",
            BinOp::CmpGe => ">=",
            BinOp::CmpLe => "<=",
        })
    }
}

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub enum IrInstr {
    Copy {
        dst: VReg,
        src: VReg,
    },
    UnOp {
        op: UnOp,
        dst: VReg,
        src: VReg,
    },
    BinOp {
        op: BinOp,
        dst: VReg,
        src1: VReg,
        src2: VReg,
    },
    LoadArg {
        dst: VReg,
        idx: usize,
    },
    LoadConst {
        dst: VReg,
        id: ConstId,
    },
    LoadVar {
        dst: VReg,
        slot: usize,
    },
    StoreVar {
        src: VReg,
        slot: usize,
    },
    Call {
        func: FuncId,
        dst: VReg,
        args: Vec<VReg>,
    },
    CallBuiltin {
        name: String,
        dst: VReg,
        args: Vec<VReg>,
    },
}

impl IrInstr {
    pub fn defs(&self) -> impl Iterator<Item = VReg> {
        let vreg = match *self {
            IrInstr::Copy { dst, .. }
            | IrInstr::UnOp { dst, .. }
            | IrInstr::BinOp { dst, .. }
            | IrInstr::LoadArg { dst, .. }
            | IrInstr::LoadConst { dst, .. }
            | IrInstr::LoadVar { dst, .. }
            | IrInstr::Call { dst, .. }
            | IrInstr::CallBuiltin { dst, .. } => Some(dst),
            _ => None,
        };

        vreg.into_iter()
    }

    pub fn uses(&self) -> impl Iterator<Item = VReg> {
        let mut res = ArrayVec::<_, 6>::new();

        match *self {
            IrInstr::Copy { src, .. }
            | IrInstr::UnOp { src, .. }
            | IrInstr::StoreVar { src, .. } => res.push(src),
            IrInstr::BinOp { src1, src2, .. } => res.extend([src1, src2]),
            IrInstr::Call { ref args, .. } | IrInstr::CallBuiltin { ref args, .. } => {
                res.extend(args.iter().copied())
            }
            _ => {}
        }

        res.into_iter()
    }

    pub fn rename_vreg(&mut self, from: VReg, to: VReg) {
        match self {
            IrInstr::Copy { dst, src } | IrInstr::UnOp { dst, src, .. } => {
                if *dst == from {
                    *dst = to;
                }
                if *src == from {
                    *src = to;
                }
            }
            IrInstr::BinOp {
                dst, src1, src2, ..
            } => {
                if *dst == from {
                    *dst = to;
                }
                if *src1 == from {
                    *src1 = to;
                }
                if *src2 == from {
                    *src2 = to;
                }
            }
            IrInstr::LoadArg { dst: vreg, .. }
            | IrInstr::LoadConst { dst: vreg, .. }
            | IrInstr::LoadVar { dst: vreg, .. }
            | IrInstr::StoreVar { src: vreg, .. } => {
                if *vreg == from {
                    *vreg = to;
                }
            }
            IrInstr::Call { dst, args, .. } | IrInstr::CallBuiltin { dst, args, .. } => {
                if *dst == from {
                    *dst = to;
                }

                for arg in args {
                    if *arg == from {
                        *arg = to;
                    }
                }
            }
        }
    }
}

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub enum Terminator {
    Unset,
    RetValue {
        src: VReg,
    },
    Jmp(BasicBlockId),
    JmpIf {
        cond: VReg,
        if_true: BasicBlockId,
        if_false: BasicBlockId,
    },
}

impl Terminator {
    pub fn uses(&self) -> impl Iterator<Item = VReg> {
        let vreg = match *self {
            Terminator::RetValue { src: vreg, .. } | Terminator::JmpIf { cond: vreg, .. } => {
                Some(vreg)
            }
            _ => None,
        };

        vreg.into_iter()
    }

    pub fn rename_vreg(&mut self, from: VReg, to: VReg) {
        match self {
            Terminator::RetValue { src: vreg } | Terminator::JmpIf { cond: vreg, .. } => {
                if *vreg == from {
                    *vreg = to;
                }
            }
            _ => {}
        }
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct BasicBlockId(pub usize);

impl Display for BasicBlockId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "bb{}", self.0)
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct BasicBlock {
    pub instrs: Vec<IrInstr>,
    pub terminator: Terminator,
    predecessors: Vec<BasicBlockId>,
}

impl BasicBlock {
    pub fn successors(&self) -> impl Iterator<Item = BasicBlockId> {
        let mut list = ArrayVec::<_, 2>::new();

        match self.terminator {
            Terminator::Jmp(target) => list.extend([target]),
            Terminator::JmpIf {
                if_true, if_false, ..
            } => list.extend([if_true, if_false]),
            _ => {}
        }

        list.into_iter()
    }

    pub fn predecessors(&self) -> impl Iterator<Item = BasicBlockId> + '_ {
        self.predecessors.iter().copied()
    }

    pub fn rename_successors(&mut self, from: BasicBlockId, to: BasicBlockId) {
        match &mut self.terminator {
            Terminator::Jmp(target) => {
                if *target == from {
                    *target = to;
                }
            }
            Terminator::JmpIf {
                if_true, if_false, ..
            } => {
                if *if_true == from {
                    *if_true = to;
                }
                if *if_false == from {
                    *if_false = to;
                }
            }
            _ => {}
        }
    }
}

impl Default for BasicBlock {
    fn default() -> BasicBlock {
        BasicBlock {
            instrs: Vec::new(),
            terminator: Terminator::Unset,
            predecessors: Vec::new(),
        }
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash, Default)]
pub struct FuncId(pub usize);

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Func {
    pub signature: FuncSignature,
    pub basic_blocks: Vec<BasicBlock>,
    pub vregs: Vec<VRegInfo>,
    pub used_slots: usize,
}

impl Func {
    pub fn new(signature: FuncSignature) -> Func {
        Func {
            signature,
            basic_blocks: Vec::new(),
            vregs: Vec::new(),
            used_slots: 0,
        }
    }

    pub fn dfs(&mut self, mut callback: impl FnMut(BasicBlockId, &mut BasicBlock)) {
        let mut visited = HashSet::new();
        let mut stack = Vec::new();
        stack.push(BasicBlockId(0));
        while let Some(bb_id) = stack.pop() {
            if !visited.insert(bb_id) {
                continue;
            }

            stack.extend(self.basic_blocks[bb_id.0].successors());
            callback(bb_id, &mut self.basic_blocks[bb_id.0]);
        }
    }

    pub fn update_predecessors(&mut self) {
        for bb in &mut self.basic_blocks {
            bb.predecessors.clear();
        }

        for src_id in 0..self.basic_blocks.len() {
            for dst_id in self.basic_blocks[src_id].successors() {
                self.basic_blocks[dst_id.0]
                    .predecessors
                    .push(BasicBlockId(src_id));
            }
        }

        for bb in &mut self.basic_blocks {
            bb.predecessors.sort();
            bb.predecessors.dedup();
        }
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct FuncSignature {
    pub name: String,
    pub args: Vec<(String, Type)>,
    pub ret: Type,
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct VRegInfo {
    pub ty: Type,
    pub reg: Option<Reg>,
}

impl Default for VRegInfo {
    fn default() -> Self {
        VRegInfo {
            ty: Type::Never,
            reg: None,
        }
    }
}

#[derive(Debug, Default, Clone)]
pub struct Builtins {
    signatures: HashMap<String, FuncSignature>,
}

impl Builtins {
    pub fn add(&mut self, name: &str, args: &[(&str, Type)], ret: Type) {
        self.signatures.insert(
            name.into(),
            FuncSignature {
                name: name.into(),
                args: args.iter().map(|&(name, ty)| (name.into(), ty)).collect(),
                ret,
            },
        );
    }

    pub fn get(&self, name: &str) -> Option<&FuncSignature> {
        self.signatures.get(name)
    }

    pub fn contains(&self, name: &str) -> bool {
        self.signatures.contains_key(name)
    }
}

#[derive(Debug)]
pub struct IrRoot {
    pub consts: Consts,
    pub builtins: Builtins,
    pub funcs: Vec<Func>,
}

impl IrRoot {
    pub fn stringify(&self) -> String {
        let mut str = String::new();

        for const_id in 0..self.consts.by_id.len() {
            let const_id = ConstId(const_id);
            str.push_str(&self.stringify_const(const_id));
            str.push('\n');
        }

        if !self.consts.by_id.is_empty() {
            str.push('\n');
        }

        for func_id in 0..self.funcs.len() {
            if func_id > 0 {
                str.push('\n');
            }

            let func_id = FuncId(func_id);
            str.push_str(&self.stringify_func(func_id));
            str.push('\n');
        }

        str
    }

    fn stringify_const(&self, const_id: ConstId) -> String {
        let value = &self.consts.by_id[const_id.0];
        format!("const {const_id} = {value:?}")
    }

    fn stringify_func(&self, func_id: FuncId) -> String {
        let mut str = format!("func {}", self.stringify_func_sig(func_id));

        for bb_id in 0..self.funcs[func_id.0].basic_blocks.len() {
            let bb_id = BasicBlockId(bb_id);
            str.push_str("\n  ");
            str.push_str(&self.stringify_basic_block(func_id, bb_id));
        }

        str
    }

    fn stringify_func_sig(&self, func_id: FuncId) -> String {
        let sig = &self.funcs[func_id.0].signature;
        let mut str = format!("({}.{}", sig.name, func_id.0);
        for (name, ty) in &sig.args {
            str.push_str(&format!(" ({name} {ty})"));
        }
        str.push_str(&format!(" {}):", sig.ret));
        str
    }

    fn stringify_basic_block(&self, func_id: FuncId, bb_id: BasicBlockId) -> String {
        let bb = &self.funcs[func_id.0].basic_blocks[bb_id.0];
        let mut str = format!("{bb_id}:");

        for instr in &bb.instrs {
            str.push_str("\n    ");
            str.push_str(&self.stringify_instr(func_id, instr));
        }

        str.push_str("\n    ");
        str.push_str(&self.stringify_terminator(func_id, &bb.terminator));

        str
    }

    fn stringify_instr(&self, func_id: FuncId, instr: &IrInstr) -> String {
        let vreg = |vreg| self.stringify_vreg_ref(func_id, vreg);

        match *instr {
            IrInstr::Copy { dst, src } => format!("{} <- {}", vreg(dst), vreg(src)),
            IrInstr::UnOp { op, dst, src } => format!("{} <- {op} {}", vreg(dst), vreg(src)),
            IrInstr::BinOp {
                op,
                dst,
                src1,
                src2,
            } => format!("{} <- {} {op} {}", vreg(dst), vreg(src1), vreg(src2)),
            IrInstr::LoadArg { dst, idx } => {
                format!("{} <- arg{idx}", vreg(dst))
            }
            IrInstr::LoadConst { dst, id } => {
                format!("{} <- {id}  // {:?}", vreg(dst), &self.consts.by_id[id.0])
            }
            IrInstr::LoadVar { dst, slot } => {
                format!("{} <- slot{slot}", vreg(dst))
            }
            IrInstr::StoreVar { src, slot } => {
                format!("slot{slot} <- {}", vreg(src))
            }
            IrInstr::Call {
                func,
                dst,
                ref args,
            } => {
                let func_id = func.0;
                let func_name = &self.funcs[func.0].signature.name;
                let mut str = format!("{} <- call {func_name}.{func_id}(", vreg(dst));
                for (i, &arg) in args.iter().enumerate() {
                    if i > 0 {
                        str.push_str(", ");
                    }
                    str.push_str(&vreg(arg));
                }
                str.push(')');
                str
            }
            IrInstr::CallBuiltin {
                ref name,
                dst,
                ref args,
            } => {
                let mut str = format!("{} <- call-builtin {name}(", vreg(dst));
                for (i, &arg) in args.iter().enumerate() {
                    if i > 0 {
                        str.push_str(", ");
                    }
                    str.push_str(&vreg(arg));
                }
                str.push(')');
                str
            }
        }
    }

    fn stringify_terminator(&self, func_id: FuncId, terminator: &Terminator) -> String {
        let vreg = |vreg| self.stringify_vreg_ref(func_id, vreg);

        match *terminator {
            Terminator::Unset => "terminator unset".into(),
            Terminator::RetValue { src } => format!("ret {}", vreg(src)),
            Terminator::Jmp(target) => format!("jmp {target}"),
            Terminator::JmpIf {
                cond,
                if_true,
                if_false,
            } => format!("if {} (jmp {if_true}) else (jmp {if_false})", vreg(cond)),
        }
    }

    fn stringify_vreg_ref(&self, func_id: FuncId, vreg: VReg) -> String {
        if let Some(reg) = self.funcs[func_id.0].vregs[vreg.0].reg {
            format!("{vreg}({reg})")
        } else {
            format!("{vreg}")
        }
    }
}
