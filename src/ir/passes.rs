use std::collections::HashMap;

use super::*;

pub fn simplify(ir: &mut IrRoot) {
    for func in &mut ir.funcs {
        reorder_blocks(func);
    }
}

fn reorder_blocks(func: &mut Func) {
    func.update_predecessors();

    let mut order = Vec::new();
    let mut marked = HashSet::new();
    let mut renames = HashMap::new();

    let mut head = BasicBlockId(0);

    loop {
        renames.insert(head, BasicBlockId(order.len()));
        order.push(head);
        marked.insert(head);

        let unmarked_successors = func.basic_blocks[head.0]
            .successors()
            .filter(|id| !marked.contains(id));

        let next_head = unmarked_successors.max_by_key(|id| {
            func.basic_blocks[id.0]
                .predecessors()
                .map(|id| if marked.contains(&id) { 1 } else { 0 })
                .sum::<i32>()
        });

        if let Some(next_head) = next_head {
            head = next_head;
            continue;
        }

        let Some(next_head) = (0..func.basic_blocks.len())
            .map(BasicBlockId)
            .filter(|bb_id| {
                let is_reachable = func.basic_blocks[bb_id.0].predecessors().count() > 0;
                !marked.contains(bb_id) && is_reachable
            })
            .max_by_key(|bb_id| func.basic_blocks[bb_id.0].successors().count())
        else {
            break;
        };

        head = next_head;
    }

    let mut new_basic_blocks = Vec::new();

    for id in order {
        let mut new_bb = func.basic_blocks[id.0].clone();

        for (&old_id, &new_id) in &renames {
            let new_id = BasicBlockId(func.basic_blocks.len() + new_id.0);
            new_bb.rename_successors(old_id, new_id);
        }

        for &new_id in renames.values() {
            let old_id = BasicBlockId(func.basic_blocks.len() + new_id.0);
            new_bb.rename_successors(old_id, new_id);
        }

        new_basic_blocks.push(new_bb);
    }

    func.basic_blocks = new_basic_blocks;
}
