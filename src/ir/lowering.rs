use std::collections::HashMap;

use anyhow::{anyhow, bail, Result};

use super::*;
use crate::isa::Reg;
use crate::syntax::*;

pub fn lower(root: &[Expr], builtins: &Builtins) -> Result<IrRoot> {
    let mut ctx = LoweringContext::new(builtins.clone());
    ctx.lower_root(root)?;
    Ok(ctx.finish())
}

#[derive(Debug)]
struct Scope<T> {
    scopes: Vec<HashMap<String, T>>,
}

impl<T> Scope<T> {
    fn push(&mut self) {
        self.scopes.push(HashMap::new());
    }

    fn get(&self, ident: &str) -> Option<T>
    where
        T: Copy,
    {
        for scope in self.scopes.iter().rev() {
            if let Some(val) = scope.get(ident) {
                return Some(*val);
            }
        }

        None
    }

    fn set(&mut self, ident: &str, value: T) {
        self.scopes.last_mut().unwrap().insert(ident.into(), value);
    }

    fn pop(&mut self) {
        self.scopes.pop();
    }
}

impl<T> Default for Scope<T> {
    fn default() -> Self {
        Scope {
            scopes: vec![HashMap::new()],
        }
    }
}

#[derive(Debug, Clone, Copy)]
struct LoopState {
    head: BasicBlockId,
    tail: BasicBlockId,
    dst: VReg,
    expected_ty: Option<Type>,
}

#[derive(Debug, Default)]
struct LoweringContext {
    consts: Consts,
    builtins: Builtins,
    funcs: Vec<Func>,
    funcs_scope: Scope<FuncId>,
    vars_scope: Scope<VReg>,
    cur_func_id: FuncId,
    cur_bb_id: BasicBlockId,
    next_vreg: VReg,
    cur_loop: Option<LoopState>,
}

impl LoweringContext {
    fn new(builtins: Builtins) -> LoweringContext {
        LoweringContext {
            builtins,
            ..Default::default()
        }
    }

    fn lower_root(&mut self, exprs: &[Expr]) -> Result<()> {
        let func_id = FuncId(0);
        self.funcs.push(Func::new(FuncSignature {
            name: "main".into(),
            args: Vec::new(),
            ret: Type::Unit,
        }));

        let body = Expr {
            kind: ExprKind::List(exprs.into()),
            range: TextRange::new(exprs[0].range.start, exprs[exprs.len() - 1].range.end),
        };

        self.lower_func(func_id, &body)
    }

    fn finish(self) -> IrRoot {
        IrRoot {
            consts: self.consts,
            builtins: self.builtins,
            funcs: self.funcs,
        }
    }

    fn lower_funcs(&mut self, exprs: &[Expr]) -> Result<()> {
        for (func_id, body) in self.collect_funcs(exprs)? {
            self.lower_func(func_id, body)?;
        }

        Ok(())
    }

    fn collect_funcs<'a>(&mut self, exprs: &'a [Expr]) -> Result<Vec<(FuncId, &'a Expr)>> {
        let mut funcs: Vec<(FuncId, &Expr)> = Vec::new();

        for expr in exprs {
            let ExprKind::List(list) = &expr.kind else {
                continue;
            };

            if list.is_empty() || !list[0].is_symbol("defun") {
                continue;
            }

            if list.len() != 3 {
                bail!("{}: 3 element list expected", expr.range);
            }

            let signature = expr_func_signature(&list[1])?;
            let body = &list[2];

            let func_id = FuncId(self.funcs.len());
            self.funcs_scope.set(&signature.name, func_id);
            funcs.push((func_id, body));
            self.funcs.push(Func::new(signature));
        }

        Ok(funcs)
    }

    fn lower_func(&mut self, func_id: FuncId, body: &Expr) -> Result<()> {
        let old_func_id = self.cur_func_id;
        let old_bb_id = self.cur_bb_id;
        let old_next_vreg = self.next_vreg;
        let old_var_scope = std::mem::take(&mut self.vars_scope);
        let old_loop = self.cur_loop;

        self.cur_func_id = func_id;
        self.cur_bb_id = BasicBlockId(0);
        self.next_vreg = VReg(0);

        let signature = self.funcs[func_id.0].signature.clone();

        self.funcs[func_id.0]
            .basic_blocks
            .push(BasicBlock::default());

        for (idx, (name, ty)) in signature.args.iter().enumerate() {
            let dst = self.alloc_vreg();
            self.set_vreg_type(dst, *ty);
            self.set_vreg_reg(dst, Reg::decode(idx as u8).unwrap());
            self.instr(IrInstr::LoadArg { dst, idx });
            self.vars_scope.set(name, dst);
        }

        let tmp = self.alloc_vreg();
        self.lower_expr(tmp, body)?;
        let ty = self.get_vreg_type(tmp);
        let ret_ty = signature.ret;

        if !ty.is(ret_ty) {
            bail!(
                "{}: expected function to return value of type `{ret_ty}`, but got `{ty}`",
                body.range
            )
        }

        if ty.is(Type::Unit) {
            self.set_terminator(Terminator::RetValue { src: tmp })
        } else {
            let src = tmp;
            let dst = self.alloc_vreg();
            self.set_vreg_type(dst, ty);
            self.set_vreg_reg(dst, Reg::R0);
            self.instr(IrInstr::Copy { src, dst });
            self.set_terminator(Terminator::RetValue { src: dst });
        }

        self.cur_func_id = old_func_id;
        self.cur_bb_id = old_bb_id;
        self.next_vreg = old_next_vreg;
        self.vars_scope = old_var_scope;
        self.cur_loop = old_loop;

        Ok(())
    }

    fn alloc_vreg(&mut self) -> VReg {
        let vreg = self.next_vreg;
        self.funcs[self.cur_func_id.0]
            .vregs
            .push(VRegInfo::default());
        self.next_vreg.0 += 1;
        vreg
    }

    fn set_vreg_type(&mut self, vreg: VReg, ty: Type) {
        self.funcs[self.cur_func_id.0].vregs[vreg.0].ty = ty;
    }

    fn set_vreg_reg(&mut self, vreg: VReg, reg: Reg) {
        self.funcs[self.cur_func_id.0].vregs[vreg.0].reg = Some(reg);
    }

    fn get_vreg_type(&self, vreg: VReg) -> Type {
        self.funcs[self.cur_func_id.0].vregs[vreg.0].ty
    }

    fn instr(&mut self, instr: IrInstr) {
        self.funcs[self.cur_func_id.0].basic_blocks[self.cur_bb_id.0]
            .instrs
            .push(instr);
    }

    fn new_basic_block(&mut self) {
        let bbs = &mut self.funcs[self.cur_func_id.0].basic_blocks;
        self.cur_bb_id = BasicBlockId(bbs.len());
        bbs.push(BasicBlock::default());
    }

    fn set_terminator(&mut self, value: Terminator) {
        let terminator =
            &mut self.funcs[self.cur_func_id.0].basic_blocks[self.cur_bb_id.0].terminator;
        if let Terminator::Unset = terminator {
            *terminator = value;
        }
    }

    fn lower_expr(&mut self, dst: VReg, expr: &Expr) -> Result<()> {
        match &expr.kind {
            ExprKind::Int(int) => self.lower_int(dst, expr.range, *int),
            ExprKind::String(str) => self.lower_string(dst, str),
            ExprKind::Symbol(sym) => self.lower_op(dst, expr.range, sym, &[]),
            ExprKind::List(list) => self.lower_list(dst, expr.range, list),
        }
    }

    fn lower_int(&mut self, dst: VReg, range: TextRange, value: i64) -> Result<()> {
        let value = i32::try_from(value)
            .map_err(|_| anyhow!("{range}: value `{value}` doesn't fit into i32"))?;
        let id = self.consts.get_or_insert(Const::I32(value));
        self.set_vreg_type(dst, Type::I32);
        self.instr(IrInstr::LoadConst { dst, id });
        Ok(())
    }

    fn lower_string(&mut self, dst: VReg, string: &str) -> Result<()> {
        let id = self.consts.get_or_insert(Const::String(string.into()));
        self.set_vreg_type(dst, Type::Str);
        self.instr(IrInstr::LoadConst { dst, id });
        Ok(())
    }

    fn lower_list(&mut self, dst: VReg, range: TextRange, list: &[Expr]) -> Result<()> {
        if !list.is_empty() && list[0].symbol().is_some() {
            let op = list[0].symbol().unwrap();
            return self.lower_op(dst, range, op, &list[1..]);
        }

        self.funcs_scope.push();

        self.lower_funcs(list)?;
        self.set_vreg_type(dst, Type::Unit);

        for expr in list {
            if let ExprKind::List(list) = &expr.kind {
                if !list.is_empty() && list[0].is_symbol("defun") {
                    continue;
                }
            }

            self.lower_expr(dst, expr)?;
        }

        self.funcs_scope.push();

        Ok(())
    }

    fn lower_op(&mut self, dst: VReg, range: TextRange, op: &str, args: &[Expr]) -> Result<()> {
        let arity = args.len();

        match (op, arity) {
            ("true", 0) => self.lower_op_bool(dst, true),
            ("false", 0) => self.lower_op_bool(dst, false),
            ("continue", 0) => self.lower_op_continue(dst, range),
            ("break", 0) => self.lower_op_break(dst, range, None),
            ("return", 0) => self.lower_op_return(dst, range, None),
            ("break", 1) => self.lower_op_break(dst, range, Some(&args[0])),
            ("return", 1) => self.lower_op_return(dst, range, Some(&args[0])),
            ("loop", 1) => self.lower_op_loop(dst, range, &args[0]),
            ("!" | "-", 1) => {
                self.lower_op_unary(dst, range, UnOp::try_from(op).unwrap(), &args[0])
            }
            ("==" | "!=" | "<" | ">" | "<=" | ">=", 2) => {
                self.lower_op_cmp(dst, range, BinOp::try_from(op).unwrap(), &args[0], &args[1])
            }
            ("set", 2) => self.lower_op_set(dst, range, &args[0], &args[1]),
            ("+" | "-" | "&" | "|" | "^" | "<<" | ">>" | "*" | "/" | "%", 2..) => {
                self.lower_op_binary(dst, range, BinOp::try_from(op).unwrap(), args)
            }
            ("let", 2) => self.lower_op_let(dst, &args[0], &args[1]),
            ("if", 2) => self.lower_op_if2(dst, range, &args[0], &args[1]),
            ("if", 3) => self.lower_op_if(dst, range, &args[0], &args[1], &args[2]),
            (_, 0) => self.lower_op_var(dst, range, op),
            (_, _) => self.lower_op_call(dst, range, op, args),
        }
    }

    fn lower_op_bool(&mut self, dst: VReg, value: bool) -> Result<()> {
        let id = self.consts.get_or_insert(Const::Bool(value));
        self.set_vreg_type(dst, Type::Bool);
        self.instr(IrInstr::LoadConst { dst, id });
        Ok(())
    }

    fn lower_op_continue(&mut self, dst: VReg, range: TextRange) -> Result<()> {
        let Some(loop_state) = self.cur_loop else {
            bail!("{range}: `continue` outside of `loop`");
        };

        self.set_vreg_type(dst, Type::Never);
        self.set_terminator(Terminator::Jmp(loop_state.head));
        self.new_basic_block();

        Ok(())
    }

    fn lower_op_break(&mut self, dst: VReg, range: TextRange, value: Option<&Expr>) -> Result<()> {
        let Some(loop_state) = self.cur_loop else {
            bail!("{range}: `break` outside of `loop`");
        };

        if let Some(expr) = value {
            self.lower_expr(loop_state.dst, expr)?;
            let ty = self.get_vreg_type(loop_state.dst);

            let loop_state = self.cur_loop.as_mut().unwrap();
            if !ty.is(Type::Never) {
                match loop_state.expected_ty {
                    Some(v) if v.is(ty) => {}
                    Some(v) => {
                        bail!("{range}: incompatible `loop` return type: `{ty}`, expected `{v}`")
                    }
                    None => loop_state.expected_ty = Some(ty),
                }
            }
        }

        self.set_vreg_type(dst, Type::Never);
        self.set_terminator(Terminator::Jmp(loop_state.tail));
        self.new_basic_block();

        Ok(())
    }

    fn lower_op_return(&mut self, dst: VReg, range: TextRange, value: Option<&Expr>) -> Result<()> {
        let tmp = self.alloc_vreg();

        let ty = if let Some(expr) = value {
            self.lower_expr(tmp, expr)?;
            self.get_vreg_type(tmp)
        } else {
            self.set_vreg_type(tmp, Type::Unit);
            Type::Unit
        };

        let ret_ty = self.funcs[self.cur_func_id.0].signature.ret;
        if !ty.is(ret_ty) {
            bail!("{range}: cannot return value of type `{ty}` from function returning `{ret_ty}`");
        }

        self.set_vreg_type(dst, Type::Never);
        self.set_terminator(Terminator::RetValue { src: tmp });
        self.new_basic_block();

        Ok(())
    }

    fn lower_op_var(&mut self, dst: VReg, range: TextRange, name: &str) -> Result<()> {
        if self.funcs_scope.get(name).is_some() || self.builtins.contains(name) {
            return self.lower_op_call(dst, range, name, &[]);
        }

        let src = self
            .vars_scope
            .get(name)
            .ok_or_else(|| anyhow!("{range}: variable `{name}` not found"))?;
        let ty = self.get_vreg_type(src);
        self.set_vreg_type(dst, ty);
        self.instr(IrInstr::Copy { dst, src });
        Ok(())
    }

    fn lower_op_call(
        &mut self,
        dst: VReg,
        range: TextRange,
        name: &str,
        args: &[Expr],
    ) -> Result<()> {
        let func = self.funcs_scope.get(name);
        let builtin = self.builtins.get(name).cloned();

        if func.is_none() && builtin.is_none() {
            bail!("{range}: function `{name}` not found");
        }

        let is_builtin = builtin.is_some();
        let signature = func
            .map(|v| self.funcs[v.0].signature.clone())
            .or(builtin)
            .unwrap()
            .clone();

        if args.len() != signature.args.len() {
            bail!(
                "{range}: function `{name}` requires {} arguments",
                signature.args.len()
            );
        }

        let mut args_vregs = Vec::with_capacity(args.len());
        for (arg_i, arg) in args.iter().enumerate() {
            let range = arg.range;

            let vreg = self.alloc_vreg();
            self.lower_expr(vreg, arg)?;
            let ty = self.get_vreg_type(vreg);

            let (arg_name, arg_ty) = &signature.args[arg_i];

            if !ty.is(*arg_ty) {
                let arg_i = arg_i + 1;
                bail!("{range}: function `{name}` requires argument {arg_i} (`{arg_name}`) of type `{arg_ty}`, got `{ty}`")
            }

            let src = vreg;
            let dst = self.alloc_vreg();
            self.set_vreg_type(dst, ty);
            self.set_vreg_reg(vreg, Reg::decode(arg_i as u8).unwrap());
            self.instr(IrInstr::Copy { dst, src });
            args_vregs.push(dst);
        }

        let tmp = self.alloc_vreg();
        self.set_vreg_type(tmp, signature.ret);
        self.set_vreg_reg(tmp, Reg::R0);
        self.set_vreg_type(dst, signature.ret);

        if is_builtin {
            self.instr(IrInstr::CallBuiltin {
                name: name.into(),
                dst: tmp,
                args: args_vregs,
            });
        } else {
            let func = func.unwrap();
            self.instr(IrInstr::Call {
                func,
                dst: tmp,
                args: args_vregs,
            });
        }

        if !signature.ret.is(Type::Unit) {
            self.instr(IrInstr::Copy { dst, src: tmp })
        }

        Ok(())
    }

    fn lower_op_loop(&mut self, dst: VReg, range: TextRange, expr: &Expr) -> Result<()> {
        let old_loop = self.cur_loop;

        let old_bb = self.cur_bb_id;
        self.new_basic_block();
        let loop_head = self.cur_bb_id;
        self.new_basic_block();
        let loop_tail = self.cur_bb_id;

        self.cur_loop = Some(LoopState {
            head: loop_head,
            tail: loop_tail,
            dst,
            expected_ty: None,
        });

        self.cur_bb_id = old_bb;
        self.set_terminator(Terminator::Jmp(loop_head));

        self.cur_bb_id = loop_head;
        let tmp = self.alloc_vreg();
        self.lower_expr(tmp, expr)?;
        self.set_terminator(Terminator::Jmp(loop_head));
        let ty = self.get_vreg_type(tmp);

        if !ty.is(Type::Unit) {
            bail!("{range}: expected type `()` for `loop` body, but got `{ty}`")
        }

        self.cur_bb_id = loop_tail;
        self.cur_loop = old_loop;

        Ok(())
    }

    fn lower_op_unary(&mut self, dst: VReg, range: TextRange, op: UnOp, expr: &Expr) -> Result<()> {
        let tmp = self.alloc_vreg();
        self.lower_expr(tmp, expr)?;
        let ty = self.get_vreg_type(tmp);

        if ty.is(Type::I32) || ty.is(Type::Bool) {
            self.instr(IrInstr::UnOp { op, dst, src: tmp });
            return Ok(());
        }

        bail!("{range}: operator `{op}` can't be applied to type `{ty}`")
    }

    fn lower_op_cmp(
        &mut self,
        dst: VReg,
        range: TextRange,
        op: BinOp,
        lhs: &Expr,
        rhs: &Expr,
    ) -> Result<()> {
        let lhs_dst = self.alloc_vreg();
        let rhs_dst = self.alloc_vreg();

        self.lower_expr(lhs_dst, lhs)?;
        self.lower_expr(rhs_dst, rhs)?;

        let lhs_ty = self.get_vreg_type(lhs_dst);
        let rhs_ty = self.get_vreg_type(rhs_dst);

        if !lhs_ty.is(rhs_ty) || !lhs_ty.is(Type::I32) {
            bail!("{range}: operator `{op}` can't be applied to types `{lhs_ty}` and `{rhs_ty}`",)
        }

        self.set_vreg_type(dst, Type::Bool);
        self.instr(IrInstr::BinOp {
            op,
            dst,
            src1: lhs_dst,
            src2: rhs_dst,
        });

        Ok(())
    }

    fn lower_op_set(&mut self, dst: VReg, range: TextRange, lhs: &Expr, rhs: &Expr) -> Result<()> {
        self.set_vreg_type(dst, Type::Unit);

        let name = expr_ident(lhs)?;
        let dst = self
            .vars_scope
            .get(&name)
            .ok_or_else(|| anyhow!("{range}: variable `{name}` not found"))?;

        let ty = self.get_vreg_type(dst);
        self.lower_expr(dst, rhs)?;
        let new_ty = self.get_vreg_type(dst);

        if !new_ty.is(ty) {
            bail!("{range}: can't assign value of type `{new_ty}` to variable of type `{ty}`");
        }

        Ok(())
    }

    fn lower_op_binary(
        &mut self,
        dst: VReg,
        range: TextRange,
        op: BinOp,
        args: &[Expr],
    ) -> Result<()> {
        let mut lhs = None;

        for (i, expr) in args.iter().enumerate() {
            let Some((lhs_vreg, lhs_ty)) = lhs else {
                let tmp = self.alloc_vreg();
                self.lower_expr(tmp, expr)?;
                lhs = Some((tmp, self.get_vreg_type(tmp)));
                continue;
            };

            let rhs_vreg = self.alloc_vreg();
            self.lower_expr(rhs_vreg, expr)?;
            let rhs_ty = self.get_vreg_type(rhs_vreg);

            if !lhs_ty.is(rhs_ty) {
                bail!("{range}: operator `{op}` can't be applied to mixed types")
            }

            if (lhs_ty == Type::Bool && !matches!(op, BinOp::And | BinOp::Or | BinOp::Xor))
                || (lhs_ty != Type::Bool && lhs_ty != Type::I32)
            {
                bail!("{range}: operator `{op}` can't be applied to type `{lhs_ty}`")
            }

            let is_last = i == args.len() - 1;
            let dst = if is_last { dst } else { self.alloc_vreg() };
            self.set_vreg_type(dst, lhs_ty);

            self.instr(IrInstr::BinOp {
                op,
                dst,
                src1: lhs_vreg,
                src2: rhs_vreg,
            });

            lhs = Some((dst, lhs_ty));
        }

        Ok(())
    }

    fn lower_op_let(&mut self, dst: VReg, varlist: &Expr, expr: &Expr) -> Result<()> {
        self.vars_scope.push();

        let list = expr_list(varlist)?;

        for expr in list {
            let (name, expr) = expr_let_var(expr)?;
            let tmp = self.alloc_vreg();
            self.lower_expr(tmp, expr)?;
            self.vars_scope.set(&name, tmp)
        }

        self.lower_expr(dst, expr)?;
        self.vars_scope.pop();

        Ok(())
    }

    fn lower_op_if2(&mut self, dst: VReg, range: TextRange, cond: &Expr, lhs: &Expr) -> Result<()> {
        let cond_vreg = self.alloc_vreg();
        self.lower_expr(cond_vreg, cond)?;
        let cond_ty = self.get_vreg_type(cond_vreg);

        if !cond_ty.is(Type::Bool) {
            bail!("{range}: expected type `bool` for `if` condition, but got `{cond_ty}`")
        }

        let start_bb = self.cur_bb_id;

        self.new_basic_block();
        let lhs_bb = self.cur_bb_id;

        let tmp = self.alloc_vreg();
        self.lower_expr(tmp, lhs)?;
        let lhs_ty = self.get_vreg_type(tmp);

        if !lhs_ty.is(Type::Unit) {
            let range = lhs.range;
            bail!("{range}: expected type `()` for 1-branch `if` body, but got `{lhs_ty}`");
        }

        self.new_basic_block();
        let end_bb = self.cur_bb_id;

        self.cur_bb_id = lhs_bb;
        self.set_terminator(Terminator::Jmp(end_bb));

        self.cur_bb_id = start_bb;
        self.set_terminator(Terminator::JmpIf {
            cond: cond_vreg,
            if_true: lhs_bb,
            if_false: end_bb,
        });

        self.set_vreg_type(dst, Type::Unit);

        self.cur_bb_id = end_bb;
        Ok(())
    }

    fn lower_op_if(
        &mut self,
        dst: VReg,
        range: TextRange,
        cond: &Expr,
        lhs: &Expr,
        rhs: &Expr,
    ) -> Result<()> {
        let cond_vreg = self.alloc_vreg();
        self.lower_expr(cond_vreg, cond)?;
        let cond_ty = self.get_vreg_type(cond_vreg);

        if !cond_ty.is(Type::Bool) {
            let range = cond.range;
            bail!("{range}: expected type `bool` for `if` condition, but got `{cond_ty}`")
        }

        let start_bb = self.cur_bb_id;

        self.new_basic_block();
        let lhs_bb = self.cur_bb_id;
        self.lower_expr(dst, lhs)?;
        let lhs_ty = self.get_vreg_type(dst);

        self.new_basic_block();
        let rhs_bb = self.cur_bb_id;
        self.lower_expr(dst, rhs)?;
        let rhs_ty = self.get_vreg_type(dst);

        if rhs_ty == Type::Never {
            self.set_vreg_type(dst, lhs_ty);
        }

        if !lhs_ty.is(rhs_ty) {
            bail!("{range}: `if` branches have incompatible types: `{lhs_ty}` and `{rhs_ty}`")
        }

        self.new_basic_block();
        let end_bb = self.cur_bb_id;

        self.cur_bb_id = lhs_bb;
        self.set_terminator(Terminator::Jmp(end_bb));

        self.cur_bb_id = rhs_bb;
        self.set_terminator(Terminator::Jmp(end_bb));

        self.cur_bb_id = start_bb;
        self.set_terminator(Terminator::JmpIf {
            cond: cond_vreg,
            if_true: lhs_bb,
            if_false: rhs_bb,
        });

        self.cur_bb_id = end_bb;
        Ok(())
    }
}

fn expr_list(expr: &Expr) -> Result<&[Expr]> {
    match &expr.kind {
        ExprKind::List(list) => Ok(list),
        _ => bail!("{}: list expected", expr.range),
    }
}

fn expr_ident(expr: &Expr) -> Result<String> {
    match &expr.kind {
        ExprKind::Symbol(sym) => {
            if matches!(
                sym.as_str(),
                "defun" | "if" | "let" | "set" | "loop" | "break" | "continue" | "true" | "false"
            ) {
                bail!("{}: symbol `{sym}` is reserved", expr.range)
            }

            Ok(sym.into())
        }
        _ => bail!("{}: ident expected", expr.range),
    }
}

fn expr_type(expr: &Expr) -> Result<Type> {
    Type::parse(expr)
}

fn expr_func_arg(expr: &Expr) -> Result<(String, Type)> {
    let list = expr_list(expr)?;
    if list.len() != 2 {
        bail!("{}: 2 element list expected", expr.range);
    }

    let ident = expr_ident(&list[0])?;
    let ty = expr_type(&list[1])?;

    Ok((ident, ty))
}

fn expr_func_signature(expr: &Expr) -> Result<FuncSignature> {
    let list = expr_list(expr)?;
    if list.len() < 2 {
        bail!("{}: list of 2 or more elements expected", expr.range);
    }

    let name = expr_ident(&list[0])?;

    let mut args = Vec::with_capacity(list.len() - 2);
    for arg in &list[1..list.len() - 1] {
        args.push(expr_func_arg(arg)?);
    }

    if args.len() > 6 {
        bail!(
            "{}: functions of more than 6 arguments are not supported",
            expr.range
        );
    }

    let ret = expr_type(&list[list.len() - 1])?;

    Ok(FuncSignature { name, args, ret })
}

fn expr_let_var(expr: &Expr) -> Result<(String, &Expr)> {
    let list = expr_list(expr)?;
    if list.len() != 2 {
        bail!("{}: 2 element list expected", expr.range);
    }

    let ident = expr_ident(&list[0])?;
    let expr = &list[1];
    Ok((ident, expr))
}
