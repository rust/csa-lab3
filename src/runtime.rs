use std::io::{Read, Write};

use anyhow::{anyhow, Context, Result};

use crate::asm::Assembler;
use crate::codegen::codegen;
use crate::ir::{lower, reg_alloc, simplify, Builtins, Type};
use crate::machine::{ControlUnit, InitialState, IoPort};
use crate::syntax::parse_root;

const DEFAULT_RUNTIME: &str = include_str!("runtime.asm");

const CODE_START: usize = 0x1000;
const STACK_END: usize = 0x80000;
const MEM_SIZE: usize = 0x100000;

pub fn builtins() -> Builtins {
    let mut builtins = Builtins::default();
    builtins.add("print", &[("s", Type::Str)], Type::Unit);
    builtins.add("println", &[("s", Type::Str)], Type::Unit);
    builtins.add("readln", &[], Type::Str);
    builtins.add("read_all", &[], Type::Str);
    builtins.add("i32_to_str", &[("n", Type::I32)], Type::Str);
    builtins
}

pub fn compile_runtime() -> Result<Assembler> {
    let mut asm = Assembler::new(0);

    for (i, line) in DEFAULT_RUNTIME.lines().enumerate() {
        asm.parse_line(line)
            .with_context(|| format!("syntax error on line {} (runtime)", i + 1))?;
    }

    Ok(asm)
}

pub fn compile_program(
    mut input: impl Read,
    asm_dump_out: Option<impl Write>,
) -> Result<Assembler> {
    let mut source = String::new();
    input.read_to_string(&mut source)?;

    let root = parse_root(&source)?;
    let mut ir = lower(&root, &builtins())?;
    simplify(&mut ir);
    reg_alloc(&mut ir);

    let mut asm = Assembler::new(CODE_START as u32);
    codegen(&mut asm, &ir)?;

    if let Some(out) = asm_dump_out {
        write_lines(out, &asm.generate_dump())?;
    }

    Ok(asm)
}

pub fn link_program(mut runtime: Assembler, mut code: Assembler) -> Result<Vec<u8>> {
    let mut memory = vec![0; MEM_SIZE];

    for (name, addr) in runtime.labels() {
        let id = code.get_label(name);
        code.set_label_addr(id, addr)?;
    }

    let (name, addr) = code
        .labels()
        .find(|v| v.0 == "main.0")
        .ok_or_else(|| anyhow!("label `main.0` not found"))?;
    let id = runtime.get_label(name);
    runtime.set_label_addr(id, addr)?;

    runtime.validate_labels()?;
    code.validate_labels()?;

    memory[0..runtime.data().len()].copy_from_slice(runtime.data());
    memory[CODE_START..CODE_START + code.data().len()].copy_from_slice(code.data());

    Ok(memory)
}

pub fn run_machine(
    memory: Vec<u8>,
    stdin: impl Read + 'static,
    stdout: impl Write + 'static,
    log_output: Option<impl Write>,
) -> Result<()> {
    let mut control_unit = ControlUnit::new(InitialState {
        memory,
        ip: 0,
        sp: STACK_END as u32,
        io_ports: vec![
            IoPort::Reader(Box::new(stdin)),
            IoPort::Writer(Box::new(stdout)),
        ],
    });

    let res = control_unit.run();

    if let Some(out) = log_output {
        write_lines(out, control_unit.log())?;
    }

    res
}

fn write_lines(mut output: impl Write, lines: &[String]) -> Result<()> {
    for line in lines {
        writeln!(output, "{line}")?;
    }
    output.flush()?;
    Ok(())
}
