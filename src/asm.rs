use std::collections::HashMap;
use std::fmt::{self, Display};
use std::fs::File;
use std::io::{BufRead, BufReader, Read};
use std::path::Path;

use anyhow::{anyhow, bail, Context, Result};
use strum::IntoEnumIterator;

use crate::isa::*;

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum AsmInstr {
    Nop,
    Hlt,
    Mov { dst: Reg, src: RegOrImm },
    Not { dst: Reg, src: Reg },
    Neg { dst: Reg, src: Reg },

    Lb { dst: Reg, src: MemAddr },
    Lh { dst: Reg, src: MemAddr },
    Lw { dst: Reg, src: MemAddr },
    Sb { dst: MemAddr, src: Reg },
    Sh { dst: MemAddr, src: Reg },
    Sw { dst: MemAddr, src: Reg },

    Lea { dst: Reg, src: MemAddr },

    Add { dst: Reg, src1: Reg, src2: RegOrImm },
    Sub { dst: Reg, src1: Reg, src2: RegOrImm },
    And { dst: Reg, src1: Reg, src2: RegOrImm },
    Or { dst: Reg, src1: Reg, src2: RegOrImm },
    Xor { dst: Reg, src1: Reg, src2: RegOrImm },
    Shl { dst: Reg, src1: Reg, src2: RegOrImm },
    Shrl { dst: Reg, src1: Reg, src2: RegOrImm },
    Shra { dst: Reg, src1: Reg, src2: RegOrImm },
    Muls { dst: Reg, src1: Reg, src2: RegOrImm },
    Divs { dst: Reg, src1: Reg, src2: RegOrImm },
    Rems { dst: Reg, src1: Reg, src2: RegOrImm },
    Mulu { dst: Reg, src1: Reg, src2: RegOrImm },
    Divu { dst: Reg, src1: Reg, src2: RegOrImm },
    Remu { dst: Reg, src1: Reg, src2: RegOrImm },

    CmpEq { dst: Reg, src1: Reg, src2: RegOrImm },
    CmpNe { dst: Reg, src1: Reg, src2: RegOrImm },
    CmpGts { dst: Reg, src1: Reg, src2: RegOrImm },
    CmpLts { dst: Reg, src1: Reg, src2: RegOrImm },
    CmpGes { dst: Reg, src1: Reg, src2: RegOrImm },
    CmpLes { dst: Reg, src1: Reg, src2: RegOrImm },
    CmpGtu { dst: Reg, src1: Reg, src2: RegOrImm },
    CmpLtu { dst: Reg, src1: Reg, src2: RegOrImm },
    CmpGeu { dst: Reg, src1: Reg, src2: RegOrImm },
    CmpLeu { dst: Reg, src1: Reg, src2: RegOrImm },

    Movz { dst: Reg, cond: Reg, src: RegOrImm },
    Movnz { dst: Reg, cond: Reg, src: RegOrImm },
    Addaz { dst: Reg, cond: Reg, src: RegOrImm },
    Addanz { dst: Reg, cond: Reg, src: RegOrImm },

    Jmp { addr: JmpAddr },
    Jz { cond: Reg, addr: JmpAddr },
    Jnz { cond: Reg, addr: JmpAddr },
    Call { addr: JmpAddr },
    Ret,

    In { dst: Reg, src: Imm },
    Out { dst: Imm, src: Reg },
}

impl AsmInstr {
    pub fn mem_addr_mut(&mut self) -> Option<&mut MemAddr> {
        match self {
            AsmInstr::Lb { src: addr, .. }
            | AsmInstr::Lh { src: addr, .. }
            | AsmInstr::Lw { src: addr, .. }
            | AsmInstr::Sb { dst: addr, .. }
            | AsmInstr::Sh { dst: addr, .. }
            | AsmInstr::Sw { dst: addr, .. } => Some(addr),
            _ => None,
        }
    }

    fn stringify(&self, label_names: &[String]) -> String {
        match self {
            AsmInstr::Nop => "nop".into(),
            AsmInstr::Hlt => "hlt".into(),
            AsmInstr::Mov { dst, src } => format!("mov {dst} <- {src}"),
            AsmInstr::Not { dst, src } => format!("not {dst} <- {src}"),
            AsmInstr::Neg { dst, src } => format!("neg {dst} <- {src}"),
            AsmInstr::Lb { dst, src } => format!("lb {dst} <- {}", src.stringify(label_names)),
            AsmInstr::Lh { dst, src } => format!("lh {dst} <- {}", src.stringify(label_names)),
            AsmInstr::Lw { dst, src } => format!("lw {dst} <- {}", src.stringify(label_names)),
            AsmInstr::Sb { dst, src } => format!("sb {} <- {src}", dst.stringify(label_names)),
            AsmInstr::Sh { dst, src } => format!("sh {} <- {src}", dst.stringify(label_names)),
            AsmInstr::Sw { dst, src } => format!("sw {} <- {src}", dst.stringify(label_names)),
            AsmInstr::Lea { dst, src } => format!("lea {dst} <- {}", src.stringify(label_names)),
            AsmInstr::Add { dst, src1, src2 } => format!("add {dst} <- {src1}, {src2}"),
            AsmInstr::Sub { dst, src1, src2 } => format!("sub {dst} <- {src1}, {src2}"),
            AsmInstr::And { dst, src1, src2 } => format!("and {dst} <- {src1}, {src2}"),
            AsmInstr::Or { dst, src1, src2 } => format!("or {dst} <- {src1}, {src2}"),
            AsmInstr::Xor { dst, src1, src2 } => format!("xor {dst} <- {src1}, {src2}"),
            AsmInstr::Shl { dst, src1, src2 } => format!("shl {dst} <- {src1}, {src2}"),
            AsmInstr::Shrl { dst, src1, src2 } => format!("shrl {dst} <- {src1}, {src2}"),
            AsmInstr::Shra { dst, src1, src2 } => format!("shra {dst} <- {src1}, {src2}"),
            AsmInstr::Muls { dst, src1, src2 } => format!("muls {dst} <- {src1}, {src2}"),
            AsmInstr::Divs { dst, src1, src2 } => format!("divs {dst} <- {src1}, {src2}"),
            AsmInstr::Rems { dst, src1, src2 } => format!("rems {dst} <- {src1}, {src2}"),
            AsmInstr::Mulu { dst, src1, src2 } => format!("mulu {dst} <- {src1}, {src2}"),
            AsmInstr::Divu { dst, src1, src2 } => format!("divu {dst} <- {src1}, {src2}"),
            AsmInstr::Remu { dst, src1, src2 } => format!("remu {dst} <- {src1}, {src2}"),
            AsmInstr::CmpEq { dst, src1, src2 } => format!("cmpeq {dst} <- {src1}, {src2}"),
            AsmInstr::CmpNe { dst, src1, src2 } => format!("cmpne {dst} <- {src1}, {src2}"),
            AsmInstr::CmpGts { dst, src1, src2 } => format!("cmpgts {dst} <- {src1}, {src2}"),
            AsmInstr::CmpLts { dst, src1, src2 } => format!("cmplts {dst} <- {src1}, {src2}"),
            AsmInstr::CmpGes { dst, src1, src2 } => format!("cmpges {dst} <- {src1}, {src2}"),
            AsmInstr::CmpLes { dst, src1, src2 } => format!("cmples {dst} <- {src1}, {src2}"),
            AsmInstr::CmpGtu { dst, src1, src2 } => format!("cmpgtu {dst} <- {src1}, {src2}"),
            AsmInstr::CmpLtu { dst, src1, src2 } => format!("cmpltu {dst} <- {src1}, {src2}"),
            AsmInstr::CmpGeu { dst, src1, src2 } => format!("cmpgeu {dst} <- {src1}, {src2}"),
            AsmInstr::CmpLeu { dst, src1, src2 } => format!("cmpleu {dst} <- {src1}, {src2}"),
            AsmInstr::Movz { dst, cond, src } => format!("movz {dst} <- {src}, {cond}"),
            AsmInstr::Movnz { dst, cond, src } => format!("movnz {dst} <- {src}, {cond}"),
            AsmInstr::Addaz { dst, cond, src } => format!("addaz {dst} <- {src}, {cond}"),
            AsmInstr::Addanz { dst, cond, src } => format!("addanz {dst} <- {src}, {cond}"),
            AsmInstr::Jmp { addr } => format!("jmp {}", addr.stringify(label_names)),
            AsmInstr::Jz { cond, addr } => {
                format!("jz {}, {cond}", addr.stringify(label_names))
            }
            AsmInstr::Jnz { cond, addr } => {
                format!("jnz {}, {cond}", addr.stringify(label_names))
            }
            AsmInstr::Call { addr } => format!("call {}", addr.stringify(label_names)),
            AsmInstr::Ret => "ret".into(),
            AsmInstr::In { dst, src } => format!("in {dst} <- {src}"),
            AsmInstr::Out { dst, src } => format!("out {dst} <- {src}"),
        }
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash, Default)]
pub struct LabelId(pub u32);

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum RegOrImm {
    Reg(Reg),
    Imm(Imm),
}

impl Display for RegOrImm {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            RegOrImm::Reg(reg) => reg.fmt(f),
            RegOrImm::Imm(imm) => imm.fmt(f),
        }
    }
}

impl From<Reg> for RegOrImm {
    fn from(reg: Reg) -> Self {
        RegOrImm::Reg(reg)
    }
}

impl From<Imm> for RegOrImm {
    fn from(imm: Imm) -> Self {
        RegOrImm::Imm(imm)
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum MemAddr {
    Abs(Reg),
    Rel(Reg, RegOrImm),
    Label(LabelId),
}

impl MemAddr {
    fn stringify(&self, label_names: &[String]) -> String {
        match self {
            MemAddr::Abs(reg) => format!("[{reg}]"),
            MemAddr::Label(label) => label_names[label.0 as usize].clone(),
            MemAddr::Rel(base, RegOrImm::Imm(offset)) if offset.0 >= 0 => {
                format!("[{base}+{offset}]")
            }
            MemAddr::Rel(base, RegOrImm::Imm(offset)) => format!("[{base}{offset}]"),
            MemAddr::Rel(base, offset) => format!("[{base}+{offset}]"),
        }
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum JmpAddr {
    Abs(Reg),
    Label(LabelId),
    RelIp(Imm),
}

impl JmpAddr {
    fn stringify(&self, label_names: &[String]) -> String {
        match self {
            JmpAddr::Abs(reg) => reg.to_string(),
            JmpAddr::Label(label) => label_names[label.0 as usize].clone(),
            JmpAddr::RelIp(imm) if imm.0 >= 0 => format!("[ip+{}]", imm),
            JmpAddr::RelIp(imm) => format!("[ip{}]", imm),
        }
    }
}

#[derive(Debug, Clone)]
enum DumpEntry {
    Newline,
    Label(LabelId),
    Instr(AsmInstr),
    U8(u8),
    U16(u16),
    U32(u32),
    I8(i8),
    I16(i16),
    I32(i32),
    String(String),
}

#[derive(Debug)]
pub struct Assembler {
    base_addr: u32,
    data: Vec<u8>,
    dump_entries: Vec<(u32, DumpEntry)>,
    labels: Vec<Option<u32>>,
    label_names: Vec<String>,
    label_references: HashMap<LabelId, Vec<u32>>,
    labels_by_name: HashMap<String, LabelId>,
}

impl Assembler {
    pub fn new(base_addr: u32) -> Assembler {
        Assembler {
            base_addr,
            data: Vec::new(),
            dump_entries: Vec::new(),
            labels: Vec::new(),
            label_names: Vec::new(),
            label_references: HashMap::new(),
            labels_by_name: HashMap::new(),
        }
    }

    pub fn data(&self) -> &[u8] {
        &self.data
    }

    fn cur_addr(&self) -> u32 {
        self.base_addr + (self.data.len() as u32)
    }

    fn add_dump_entry(&mut self, entry: DumpEntry) {
        self.dump_entries.push((self.cur_addr(), entry));
    }

    pub fn newline(&mut self) {
        self.add_dump_entry(DumpEntry::Newline);
    }

    pub fn get_label(&mut self, name: &str) -> LabelId {
        if let Some(&id) = self.labels_by_name.get(name) {
            return id;
        };

        let id = LabelId(self.labels.len() as u32);

        self.labels.push(None);
        self.label_names.push(name.into());
        self.labels_by_name.insert(name.into(), id);

        id
    }

    pub fn set_label(&mut self, id: LabelId) -> Result<()> {
        let cur_addr = self.cur_addr();

        self.add_dump_entry(DumpEntry::Label(id));
        self.set_label_addr(id, cur_addr)?;

        Ok(())
    }

    pub fn labels(&self) -> impl Iterator<Item = (&str, u32)> + '_ {
        self.labels_by_name.iter().flat_map(|(name, id)| {
            self.labels
                .get(id.0 as usize)
                .and_then(|v| *v)
                .map(|addr| (name.as_str(), addr))
        })
    }

    pub fn set_label_addr(&mut self, id: LabelId, set_addr: u32) -> Result<()> {
        if self.labels[id.0 as usize].is_some() {
            bail!("duplicate label: {}", self.label_names[id.0 as usize]);
        }

        self.labels[id.0 as usize] = Some(set_addr);

        if let Some(refs) = self.label_references.remove(&id) {
            for addr in refs {
                let offset = (set_addr as i64) - (addr as i64) - 4;
                let [b0, b1] = i16::try_from(offset)
                    .map_err(|_| anyhow!("label too far: {}", self.label_names[id.0 as usize]))?
                    .to_le_bytes();
                self.data[(addr - self.base_addr) as usize + 2] = b0;
                self.data[(addr - self.base_addr) as usize + 3] = b1;
            }
        }

        Ok(())
    }

    pub fn u8(&mut self, val: u8) {
        self.add_dump_entry(DumpEntry::U8(val));
        self.data.push(val);
    }

    pub fn i8(&mut self, val: i8) {
        self.add_dump_entry(DumpEntry::I8(val));
        self.data.push(val as u8);
    }

    pub fn u16(&mut self, val: u16) {
        self.add_dump_entry(DumpEntry::U16(val));
        self.data.extend(val.to_le_bytes());
    }

    pub fn i16(&mut self, val: i16) {
        self.add_dump_entry(DumpEntry::I16(val));
        self.data.extend(val.to_le_bytes());
    }

    pub fn u32(&mut self, val: u32) {
        self.add_dump_entry(DumpEntry::U32(val));
        self.data.extend(val.to_le_bytes());
    }

    pub fn i32(&mut self, val: i32) {
        self.add_dump_entry(DumpEntry::I32(val));
        self.data.extend(val.to_le_bytes());
    }

    pub fn string(&mut self, val: &str) {
        self.add_dump_entry(DumpEntry::String(val.into()));
        self.data.extend((val.len() as u32).to_le_bytes());
        self.data.extend(val.as_bytes());
    }

    pub fn instr(&mut self, instr: AsmInstr) -> Result<()> {
        self.add_dump_entry(DumpEntry::Instr(instr));

        match instr {
            // general ops
            AsmInstr::Nop => self.encode_instr(GeneralOp::Nop, Reg::R0, Reg::R0, Reg::R0),
            AsmInstr::Hlt => self.encode_instr(GeneralOp::Hlt, Reg::R0, Reg::R0, Reg::R0),
            AsmInstr::Not { dst, src } => self.encode_instr(GeneralOp::Not, dst, Reg::R0, src),
            AsmInstr::Neg { dst, src } => self.encode_instr(GeneralOp::Neg, dst, Reg::R0, src),
            AsmInstr::Mov { dst, src } => self.encode_instr(GeneralOp::Mov, dst, Reg::R0, src),

            // mem ops
            AsmInstr::Lb { dst, src } => self.encode_ld_instr(MemSize::Byte, dst, src)?,
            AsmInstr::Lh { dst, src } => self.encode_ld_instr(MemSize::Half, dst, src)?,
            AsmInstr::Lw { dst, src } => self.encode_ld_instr(MemSize::Word, dst, src)?,
            AsmInstr::Sb { dst, src } => self.encode_st_instr(MemSize::Byte, dst, src)?,
            AsmInstr::Sh { dst, src } => self.encode_st_instr(MemSize::Half, dst, src)?,
            AsmInstr::Sw { dst, src } => self.encode_st_instr(MemSize::Word, dst, src)?,

            // lea macro
            AsmInstr::Lea { dst, src } => match src {
                MemAddr::Abs(reg) => self.encode_instr(GeneralOp::Mov, dst, Reg::R0, reg),
                MemAddr::Rel(base, offset) => self.encode_instr(AluOp::Add, dst, base, offset),
                MemAddr::Label(label) => {
                    self.encode_instr_label(AluOp::Add, dst, Reg::Ip, label)?
                }
            },

            // alu ops
            AsmInstr::Add { dst, src1, src2 } => self.encode_instr(AluOp::Add, dst, src1, src2),
            AsmInstr::Sub { dst, src1, src2 } => self.encode_instr(AluOp::Sub, dst, src1, src2),
            AsmInstr::And { dst, src1, src2 } => self.encode_instr(AluOp::And, dst, src1, src2),
            AsmInstr::Or { dst, src1, src2 } => self.encode_instr(AluOp::Or, dst, src1, src2),
            AsmInstr::Xor { dst, src1, src2 } => self.encode_instr(AluOp::Xor, dst, src1, src2),
            AsmInstr::Shl { dst, src1, src2 } => self.encode_instr(AluOp::Shl, dst, src1, src2),
            AsmInstr::Shrl { dst, src1, src2 } => self.encode_instr(AluOp::Shrl, dst, src1, src2),
            AsmInstr::Shra { dst, src1, src2 } => self.encode_instr(AluOp::Shra, dst, src1, src2),
            AsmInstr::Muls { dst, src1, src2 } => self.encode_instr(AluOp::Muls, dst, src1, src2),
            AsmInstr::Divs { dst, src1, src2 } => self.encode_instr(AluOp::Divs, dst, src1, src2),
            AsmInstr::Rems { dst, src1, src2 } => self.encode_instr(AluOp::Rems, dst, src1, src2),
            AsmInstr::Mulu { dst, src1, src2 } => self.encode_instr(AluOp::Mulu, dst, src1, src2),
            AsmInstr::Divu { dst, src1, src2 } => self.encode_instr(AluOp::Divu, dst, src1, src2),
            AsmInstr::Remu { dst, src1, src2 } => self.encode_instr(AluOp::Remu, dst, src1, src2),

            // cmp ops
            AsmInstr::CmpEq { dst, src1, src2 } => self.encode_instr(CmpOp::Eq, dst, src1, src2),
            AsmInstr::CmpNe { dst, src1, src2 } => self.encode_instr(CmpOp::Ne, dst, src1, src2),
            AsmInstr::CmpGts { dst, src1, src2 } => self.encode_instr(CmpOp::Gts, dst, src1, src2),
            AsmInstr::CmpLts { dst, src1, src2 } => self.encode_instr(CmpOp::Lts, dst, src1, src2),
            AsmInstr::CmpGes { dst, src1, src2 } => self.encode_instr(CmpOp::Ges, dst, src1, src2),
            AsmInstr::CmpLes { dst, src1, src2 } => self.encode_instr(CmpOp::Les, dst, src1, src2),
            AsmInstr::CmpGtu { dst, src1, src2 } => self.encode_instr(CmpOp::Gtu, dst, src1, src2),
            AsmInstr::CmpLtu { dst, src1, src2 } => self.encode_instr(CmpOp::Ltu, dst, src1, src2),
            AsmInstr::CmpGeu { dst, src1, src2 } => self.encode_instr(CmpOp::Geu, dst, src1, src2),
            AsmInstr::CmpLeu { dst, src1, src2 } => self.encode_instr(CmpOp::Leu, dst, src1, src2),

            // cond ops
            AsmInstr::Movz { dst, cond, src } => self.encode_instr(
                CondOp {
                    is_zero: true,
                    kind: CondOpKind::Mov,
                },
                dst,
                cond,
                src,
            ),
            AsmInstr::Movnz { dst, cond, src } => self.encode_instr(
                CondOp {
                    is_zero: false,
                    kind: CondOpKind::Mov,
                },
                dst,
                cond,
                src,
            ),
            AsmInstr::Addaz { dst, cond, src } => self.encode_instr(
                CondOp {
                    is_zero: true,
                    kind: CondOpKind::Adda,
                },
                dst,
                cond,
                src,
            ),
            AsmInstr::Addanz { dst, cond, src } => self.encode_instr(
                CondOp {
                    is_zero: false,
                    kind: CondOpKind::Adda,
                },
                dst,
                cond,
                src,
            ),

            // io ops
            AsmInstr::In { dst, src } => self.encode_instr(IoOp::In, dst, Reg::R0, src),
            AsmInstr::Out { dst, src } => self.encode_instr(IoOp::Out, Reg::R0, src, dst),

            // jump ops (macros)
            AsmInstr::Jmp { addr } => match addr {
                JmpAddr::Abs(reg) => self.encode_instr(GeneralOp::Mov, Reg::Ip, Reg::R0, reg),
                JmpAddr::RelIp(imm) => self.encode_instr(AluOp::Add, Reg::Ip, Reg::Ip, imm),
                JmpAddr::Label(label) => {
                    self.encode_instr_label(AluOp::Add, Reg::Ip, Reg::Ip, label)?
                }
            },

            AsmInstr::Jz { cond, addr } | AsmInstr::Jnz { cond, addr } => {
                let is_zero = matches!(instr, AsmInstr::Jz { .. });
                match addr {
                    JmpAddr::Abs(reg) => self.encode_instr(
                        CondOp {
                            is_zero,
                            kind: CondOpKind::Mov,
                        },
                        Reg::Ip,
                        cond,
                        reg,
                    ),
                    JmpAddr::RelIp(imm) => self.encode_instr(
                        CondOp {
                            is_zero,
                            kind: CondOpKind::Adda,
                        },
                        Reg::Ip,
                        cond,
                        imm,
                    ),
                    JmpAddr::Label(label) => self.encode_instr_label(
                        CondOp {
                            is_zero,
                            kind: CondOpKind::Adda,
                        },
                        Reg::Ip,
                        cond,
                        label,
                    )?,
                }
            }

            AsmInstr::Call { addr } => match addr {
                JmpAddr::Abs(reg) => self.encode_instr(GeneralOp::Call, Reg::R0, Reg::R0, reg),
                JmpAddr::RelIp(imm) => self.encode_instr(GeneralOp::Call, Reg::R0, Reg::R0, imm),
                JmpAddr::Label(label) => {
                    self.encode_instr_label(GeneralOp::Call, Reg::R0, Reg::R0, label)?
                }
            },

            AsmInstr::Ret => self.encode_instr(GeneralOp::Mov, Reg::Ip, Reg::R0, Reg::Ra),
        }

        Ok(())
    }

    fn encode_ld_instr(&mut self, size: MemSize, dst: Reg, src: MemAddr) -> Result<()> {
        let is_store = false;
        match src {
            MemAddr::Abs(reg) => self.encode_instr(MemOp { is_store, size }, dst, reg, Imm(0)),
            MemAddr::Rel(base, offset) => {
                self.encode_instr(MemOp { is_store, size }, dst, base, offset)
            }
            MemAddr::Label(label) => {
                self.encode_instr_label(MemOp { is_store, size }, dst, Reg::Ip, label)?
            }
        }
        Ok(())
    }

    fn encode_st_instr(&mut self, size: MemSize, dst: MemAddr, src: Reg) -> Result<()> {
        let is_store = true;
        match dst {
            MemAddr::Abs(reg) => self.encode_instr(MemOp { is_store, size }, src, reg, Imm(0)),
            MemAddr::Rel(base, offset) => {
                self.encode_instr(MemOp { is_store, size }, src, base, offset)
            }
            MemAddr::Label(label) => {
                self.encode_instr_label(MemOp { is_store, size }, src, Reg::Ip, label)?
            }
        }
        Ok(())
    }

    fn encode_instr(
        &mut self,
        opcode: impl Into<Opcode>,
        dst: Reg,
        src1: Reg,
        src2_or_imm: impl Into<RegOrImm>,
    ) {
        let opcode = opcode.into();
        let instr = match src2_or_imm.into() {
            RegOrImm::Reg(reg) => Instr {
                has_imm: false,
                opcode,
                rd: dst,
                rs1: src1,
                rs2: reg,
                imm: Imm(0),
            },
            RegOrImm::Imm(imm) => Instr {
                has_imm: true,
                opcode,
                rd: dst,
                rs1: src1,
                rs2: Reg::R0,
                imm,
            },
        };

        self.data.extend(instr.encode().to_le_bytes());
    }

    fn encode_instr_label(
        &mut self,
        opcode: impl Into<Opcode>,
        dst: Reg,
        src1: Reg,
        label: LabelId,
    ) -> Result<()> {
        let cur_addr = self.cur_addr();

        let imm = match self.labels[label.0 as usize] {
            Some(addr) => {
                let offset = (addr as i64) - (cur_addr as i64) - 4;
                Imm(i16::try_from(offset).map_err(|_| {
                    anyhow!("label too far: {}", self.label_names[label.0 as usize])
                })?)
            }
            None => {
                self.label_references
                    .entry(label)
                    .or_default()
                    .push(cur_addr);
                Imm(0)
            }
        };

        let opcode = opcode.into();
        let instr = Instr {
            has_imm: true,
            opcode,
            rd: dst,
            rs1: src1,
            rs2: Reg::R0,
            imm,
        };

        self.data.extend(instr.encode().to_le_bytes());

        Ok(())
    }

    pub fn generate_dump(&self) -> Vec<String> {
        let mut lines = Vec::new();

        for (addr, entry) in &self.dump_entries {
            let b = &self.data[(addr - self.base_addr) as usize..];

            let line = match &entry {
                DumpEntry::Newline => "".into(),
                DumpEntry::Label(id) => format!("{}:", self.label_names[id.0 as usize]),
                DumpEntry::Instr(instr) => {
                    let mnemonic = instr.stringify(&self.label_names);
                    format!(
                        "  {addr:08x} - {:02x} {:02x} {:02x} {:02x} - {mnemonic}",
                        b[0], b[1], b[2], b[3]
                    )
                }
                DumpEntry::U8(v) => format!("  {addr:08x} - {:02x} .. .. .. - u8 {v}", b[0],),
                DumpEntry::I8(v) => format!("  {addr:08x} - {:02x} .. .. .. - i8 {v}", b[0],),
                DumpEntry::U16(v) => {
                    format!("  {addr:08x} - {:02x} {:02x} .. .. - u16 {v}", b[0], b[1])
                }
                DumpEntry::I16(v) => {
                    format!("  {addr:08x} - {:02x} {:02x} .. .. - i16 {v}", b[0], b[1])
                }
                DumpEntry::U32(v) => {
                    format!(
                        "  {addr:08x} - {:02x} {:02x} {:02x} {:02x} - u32 {v}",
                        b[0], b[1], b[2], b[3]
                    )
                }
                DumpEntry::I32(v) => {
                    format!(
                        "  {addr:08x} - {:02x} {:02x} {:02x} {:02x} - i32 {v}",
                        b[0], b[1], b[2], b[3]
                    )
                }
                DumpEntry::String(s) => {
                    lines.push(format!(
                        "  {addr:08x} - {:02x} {:02x} {:02x} {:02x} - string {s:?}",
                        b[0], b[1], b[2], b[3]
                    ));

                    for i in 0..s.len() / 4 {
                        let b = &b[4 + i * 4..];
                        let addr = addr + 4 + (i as u32) * 4;
                        lines.push(format!(
                            "  {addr:08x} - {:02x} {:02x} {:02x} {:02x}",
                            b[0], b[1], b[2], b[3]
                        ));
                    }

                    let b = &b[4 + s.len() / 4 * 4..];
                    let addr = addr + 4 + (s.len() as u32) / 4 * 4;
                    lines.push(match s.len() % 4 {
                        1 => format!("  {addr:08x} - {:02x}", b[0],),
                        2 => format!("  {addr:08x} - {:02x} {:02x}", b[0], b[1]),
                        3 => format!("  {addr:08x} - {:02x} {:02x} {:02x}", b[0], b[1], b[2]),
                        _ => continue,
                    });

                    continue;
                }
            };

            lines.push(line);
        }

        lines
    }

    pub fn parse_file(&mut self, path: &Path) -> Result<()> {
        let mut file =
            File::open(path).with_context(|| format!("failed to open {}", path.display()))?;
        self.parse_reader(&mut file)
    }

    pub fn parse_reader(&mut self, reader: &mut dyn Read) -> Result<()> {
        let reader = BufReader::new(reader);

        for (line_i, line) in reader.lines().enumerate() {
            let line = line?;
            self.parse_line(&line)
                .with_context(|| format!("syntax error on line {}", line_i + 1))?;
        }

        Ok(())
    }

    pub fn parse_line(&mut self, line: &str) -> Result<()> {
        let line = line
            .split_once(';')
            .map(|(line, _comm)| line)
            .unwrap_or(line);

        if line.trim().is_empty() {
            self.newline();
            return Ok(());
        }

        let (label, line) = line
            .split_once(':')
            .map(|(label, line)| (Some(label.trim()), line.trim()))
            .unwrap_or((None, line.trim()));

        if let Some(label) = label {
            let id = self.get_label(label);
            self.set_label(id)?;
        }

        if line.trim().is_empty() {
            return Ok(());
        }

        let (mnemonic, mut s) = line.split_once(' ').unwrap_or((line, ""));
        let s = &mut s;

        let instr = match mnemonic {
            "u8" => {
                let num = number(s)?;
                self.u8(u8::try_from(num).map_err(|_| anyhow!("{num} doesn't fit into u8"))?);
                return Ok(());
            }
            "i8" => {
                let num = number(s)?;
                self.i8(i8::try_from(num).map_err(|_| anyhow!("{num} doesn't fit into i8"))?);
                return Ok(());
            }
            "u16" => {
                let num = number(s)?;
                self.u16(u16::try_from(num).map_err(|_| anyhow!("{num} doesn't fit into u16"))?);
                return Ok(());
            }
            "i16" => {
                let num = number(s)?;
                self.i16(i16::try_from(num).map_err(|_| anyhow!("{num} doesn't fit into i16"))?);
                return Ok(());
            }
            "u32" => {
                let num = number(s)?;
                self.u32(u32::try_from(num).map_err(|_| anyhow!("{num} doesn't fit into u32"))?);
                return Ok(());
            }
            "i32" => {
                let num = number(s)?;
                self.i32(i32::try_from(num).map_err(|_| anyhow!("{num} doesn't fit into i32"))?);
                return Ok(());
            }

            "string" => {
                self.string(&string(s)?);
                return Ok(());
            }

            "nop" => AsmInstr::Nop,
            "hlt" => AsmInstr::Hlt,
            "mov" => AsmInstr::Mov {
                dst: (reg(s)?, arrow(s)?).0,
                src: reg_or_imm(s)?,
            },
            "not" => AsmInstr::Not {
                dst: (reg(s)?, arrow(s)?).0,
                src: reg(s)?,
            },
            "neg" => AsmInstr::Neg {
                dst: (reg(s)?, arrow(s)?).0,
                src: reg(s)?,
            },
            "lb" => AsmInstr::Lb {
                dst: (reg(s)?, arrow(s)?).0,
                src: mem_addr(self, s)?,
            },
            "lh" => AsmInstr::Lh {
                dst: (reg(s)?, arrow(s)?).0,
                src: mem_addr(self, s)?,
            },
            "lw" => AsmInstr::Lw {
                dst: (reg(s)?, arrow(s)?).0,
                src: mem_addr(self, s)?,
            },
            "sb" => AsmInstr::Sb {
                dst: (mem_addr(self, s)?, arrow(s)?).0,
                src: reg(s)?,
            },
            "sh" => AsmInstr::Sh {
                dst: (mem_addr(self, s)?, arrow(s)?).0,
                src: reg(s)?,
            },
            "sw" => AsmInstr::Sw {
                dst: (mem_addr(self, s)?, arrow(s)?).0,
                src: reg(s)?,
            },
            "lea" => AsmInstr::Lea {
                dst: (reg(s)?, arrow(s)?).0,
                src: mem_addr(self, s)?,
            },
            "add" => AsmInstr::Add {
                dst: (reg(s)?, arrow(s)?).0,
                src1: (reg(s)?, comma(s)?).0,
                src2: reg_or_imm(s)?,
            },
            "sub" => AsmInstr::Sub {
                dst: (reg(s)?, arrow(s)?).0,
                src1: (reg(s)?, comma(s)?).0,
                src2: reg_or_imm(s)?,
            },
            "and" => AsmInstr::And {
                dst: (reg(s)?, arrow(s)?).0,
                src1: (reg(s)?, comma(s)?).0,
                src2: reg_or_imm(s)?,
            },
            "or" => AsmInstr::Or {
                dst: (reg(s)?, arrow(s)?).0,
                src1: (reg(s)?, comma(s)?).0,
                src2: reg_or_imm(s)?,
            },
            "xor" => AsmInstr::Xor {
                dst: (reg(s)?, arrow(s)?).0,
                src1: (reg(s)?, comma(s)?).0,
                src2: reg_or_imm(s)?,
            },
            "shl" => AsmInstr::Shl {
                dst: (reg(s)?, arrow(s)?).0,
                src1: (reg(s)?, comma(s)?).0,
                src2: reg_or_imm(s)?,
            },
            "shrl" => AsmInstr::Shrl {
                dst: (reg(s)?, arrow(s)?).0,
                src1: (reg(s)?, comma(s)?).0,
                src2: reg_or_imm(s)?,
            },
            "shra" => AsmInstr::Shra {
                dst: (reg(s)?, arrow(s)?).0,
                src1: (reg(s)?, comma(s)?).0,
                src2: reg_or_imm(s)?,
            },
            "muls" => AsmInstr::Muls {
                dst: (reg(s)?, arrow(s)?).0,
                src1: (reg(s)?, comma(s)?).0,
                src2: reg_or_imm(s)?,
            },
            "divs" => AsmInstr::Divs {
                dst: (reg(s)?, arrow(s)?).0,
                src1: (reg(s)?, comma(s)?).0,
                src2: reg_or_imm(s)?,
            },
            "rems" => AsmInstr::Rems {
                dst: (reg(s)?, arrow(s)?).0,
                src1: (reg(s)?, comma(s)?).0,
                src2: reg_or_imm(s)?,
            },
            "mulu" => AsmInstr::Mulu {
                dst: (reg(s)?, arrow(s)?).0,
                src1: (reg(s)?, comma(s)?).0,
                src2: reg_or_imm(s)?,
            },
            "divu" => AsmInstr::Divu {
                dst: (reg(s)?, arrow(s)?).0,
                src1: (reg(s)?, comma(s)?).0,
                src2: reg_or_imm(s)?,
            },
            "remu" => AsmInstr::Remu {
                dst: (reg(s)?, arrow(s)?).0,
                src1: (reg(s)?, comma(s)?).0,
                src2: reg_or_imm(s)?,
            },
            "cmpeq" => AsmInstr::CmpEq {
                dst: (reg(s)?, arrow(s)?).0,
                src1: (reg(s)?, comma(s)?).0,
                src2: reg_or_imm(s)?,
            },
            "cmpne" => AsmInstr::CmpNe {
                dst: (reg(s)?, arrow(s)?).0,
                src1: (reg(s)?, comma(s)?).0,
                src2: reg_or_imm(s)?,
            },
            "cmpgts" => AsmInstr::CmpGts {
                dst: (reg(s)?, arrow(s)?).0,
                src1: (reg(s)?, comma(s)?).0,
                src2: reg_or_imm(s)?,
            },
            "cmplts" => AsmInstr::CmpLts {
                dst: (reg(s)?, arrow(s)?).0,
                src1: (reg(s)?, comma(s)?).0,
                src2: reg_or_imm(s)?,
            },
            "cmpges" => AsmInstr::CmpGes {
                dst: (reg(s)?, arrow(s)?).0,
                src1: (reg(s)?, comma(s)?).0,
                src2: reg_or_imm(s)?,
            },
            "cmples" => AsmInstr::CmpLes {
                dst: (reg(s)?, arrow(s)?).0,
                src1: (reg(s)?, comma(s)?).0,
                src2: reg_or_imm(s)?,
            },
            "cmpgtu" => AsmInstr::CmpGtu {
                dst: (reg(s)?, arrow(s)?).0,
                src1: (reg(s)?, comma(s)?).0,
                src2: reg_or_imm(s)?,
            },
            "cmpltu" => AsmInstr::CmpLtu {
                dst: (reg(s)?, arrow(s)?).0,
                src1: (reg(s)?, comma(s)?).0,
                src2: reg_or_imm(s)?,
            },
            "cmpgeu" => AsmInstr::CmpGeu {
                dst: (reg(s)?, arrow(s)?).0,
                src1: (reg(s)?, comma(s)?).0,
                src2: reg_or_imm(s)?,
            },
            "cmpleu" => AsmInstr::CmpLeu {
                dst: (reg(s)?, arrow(s)?).0,
                src1: (reg(s)?, comma(s)?).0,
                src2: reg_or_imm(s)?,
            },
            "movz" => AsmInstr::Movz {
                dst: (reg(s)?, arrow(s)?).0,
                src: (reg_or_imm(s)?, comma(s)?).0,
                cond: reg(s)?,
            },
            "movnz" => AsmInstr::Movnz {
                dst: (reg(s)?, arrow(s)?).0,
                src: (reg_or_imm(s)?, comma(s)?).0,
                cond: reg(s)?,
            },
            "addaz" => AsmInstr::Addaz {
                dst: (reg(s)?, arrow(s)?).0,
                src: (reg_or_imm(s)?, comma(s)?).0,
                cond: reg(s)?,
            },
            "addanz" => AsmInstr::Addanz {
                dst: (reg(s)?, arrow(s)?).0,
                src: (reg_or_imm(s)?, comma(s)?).0,
                cond: reg(s)?,
            },
            "jmp" => AsmInstr::Jmp {
                addr: jmp_addr(self, s)?,
            },
            "jz" => AsmInstr::Jz {
                addr: (jmp_addr(self, s)?, comma(s)?).0,
                cond: reg(s)?,
            },
            "jnz" => AsmInstr::Jnz {
                addr: (jmp_addr(self, s)?, comma(s)?).0,
                cond: reg(s)?,
            },
            "call" => AsmInstr::Call {
                addr: jmp_addr(self, s)?,
            },
            "ret" => AsmInstr::Ret,
            "in" => AsmInstr::In {
                dst: (reg(s)?, arrow(s)?).0,
                src: imm(s)?,
            },
            "out" => AsmInstr::Out {
                dst: (imm(s)?, arrow(s)?).0,
                src: reg(s)?,
            },
            _ => bail!("unknown mnemonic: {mnemonic}"),
        };

        if !s.trim().is_empty() {
            bail!("trailing characters");
        }

        self.instr(instr)
    }

    pub fn validate_labels(&mut self) -> Result<()> {
        if let Some(id) = self.label_references.keys().next() {
            let name = &self.label_names[id.0 as usize];
            bail!("unset label: {name}");
        }

        Ok(())
    }
}

fn word<'a>(s: &mut &'a str) -> Result<&'a str> {
    let original = *s;
    *s = s.trim_start();
    let mut len = 0;
    while s[len..]
        .chars()
        .next()
        .is_some_and(|c| c.is_ascii_alphanumeric() || c == '.' || c == '_')
    {
        len += 1;
    }
    let word = &s[..len];
    if word.is_empty() {
        *s = original;
        bail!("word expected")
    }
    *s = &s[len..];
    Ok(word)
}

fn token(s: &mut &str, expect: &str) -> Result<()> {
    let original = *s;
    *s = s.trim_start();
    if s.starts_with(expect) {
        *s = &s[expect.len()..];
        Ok(())
    } else {
        *s = original;
        bail!("`{expect}` expected")
    }
}

fn comma(s: &mut &str) -> Result<()> {
    token(s, ",")
}

fn arrow(s: &mut &str) -> Result<()> {
    token(s, "<-")
}

fn reg(s: &mut &str) -> Result<Reg> {
    let original = *s;
    let word = word(s)?;
    for reg in Reg::iter() {
        if word.eq_ignore_ascii_case(reg.into()) {
            return Ok(reg);
        }
    }
    *s = original;
    bail!("unknown reg: {word}")
}

fn number(s: &mut &str) -> Result<i64> {
    let original = *s;

    *s = s.trim_start();

    let negative = if s.starts_with('-') {
        *s = &s[1..];
        true
    } else {
        false
    };

    *s = s.trim_start();

    let word = word(s)?;
    let res = if let Some(rest) = word.strip_prefix("0x") {
        i64::from_str_radix(rest, 16)
    } else if let Some(rest) = word.strip_prefix("0o") {
        i64::from_str_radix(rest, 8)
    } else if let Some(rest) = word.strip_prefix("0b") {
        i64::from_str_radix(rest, 2)
    } else {
        word.parse()
    };

    if res.is_err() {
        *s = original;
    }

    let res = res.map_err(|_| anyhow!("invalid number: {word}"))?;
    Ok(if negative { -res } else { res })
}

fn string(s: &mut &str) -> Result<String> {
    *s = s.trim_start();
    token(s, "\"")?;
    let mut len = 0;
    while let Some(idx) = s.find('\"') {
        if idx == 0 || &s[idx - 1..idx] != "\\" {
            len = idx;
            break;
        }
    }
    let value = s[..len].to_string().replace("\\\"", "\"");
    *s = &s[len..];
    token(s, "\"")?;

    Ok(value)
}

fn imm(s: &mut &str) -> Result<Imm> {
    let original = *s;
    let num = number(s)?;
    if i16::try_from(num).is_err() && u16::try_from(num).is_err() {
        *s = original;
        bail!("imm is to large to fit into 16 bits: {num}");
    }

    Ok(Imm(num as i16))
}

fn reg_or_imm(s: &mut &str) -> Result<RegOrImm> {
    reg(s)
        .map(RegOrImm::Reg)
        .or_else(|_| imm(s).map(RegOrImm::Imm))
        .map_err(|_| anyhow!("reg or imm expected"))
}

fn jmp_addr(asm: &mut Assembler, s: &mut &str) -> Result<JmpAddr> {
    if let Ok(reg) = reg(s) {
        return Ok(JmpAddr::Abs(reg));
    }

    *s = s.trim_start();
    if s.starts_with("[ip") {
        token(s, "[ip")?;
        *s = s.trim_start();
        if s.starts_with('+') {
            *s = &s[1..];
        }
        let imm = imm(s)?;
        token(s, "]")?;
        return Ok(JmpAddr::RelIp(imm));
    }

    word(s)
        .map(|name| JmpAddr::Label(asm.get_label(name)))
        .map_err(|_| anyhow!("jmp addr expected"))
}

fn mem_addr(asm: &mut Assembler, s: &mut &str) -> Result<MemAddr> {
    *s = s.trim_start();
    if s.starts_with('[') {
        token(s, "[")?;
        let base = reg(s)?;
        *s = s.trim_start();
        let offset = if s.starts_with(']') {
            Imm(0).into()
        } else {
            if !s.starts_with('-') {
                token(s, "+")?;
            }
            reg_or_imm(s)?
        };
        token(s, "]")?;
        return Ok(MemAddr::Rel(base, offset));
    }

    word(s)
        .map(|name| MemAddr::Label(asm.get_label(name)))
        .map_err(|_| anyhow!("mem addr expected"))
}
