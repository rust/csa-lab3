use std::fmt::{self, Display};

use anyhow::{anyhow, Error, Result};

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub struct TextPos {
    pub line: u32,
    pub col: u32,
    pub byte: usize,
}

impl TextPos {
    pub fn new(line: u32, col: u32, byte: usize) -> TextPos {
        TextPos { line, col, byte }
    }
}

impl Display for TextPos {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}:{}", self.line, self.col)
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub struct TextRange {
    pub start: TextPos,
    pub end: TextPos,
}

impl TextRange {
    pub fn new(start: TextPos, end: TextPos) -> TextRange {
        TextRange { start, end }
    }
}

impl Display for TextRange {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}-{}", self.start, self.end)
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Expr {
    pub kind: ExprKind,
    pub range: TextRange,
}

impl Expr {
    pub fn symbol(&self) -> Option<&str> {
        let ExprKind::Symbol(s) = &self.kind else {
            return None;
        };

        Some(s)
    }

    pub fn is_symbol(&self, symbol: &str) -> bool {
        self.symbol() == Some(symbol)
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum ExprKind {
    Int(i64),
    Symbol(String),
    String(String),
    List(Vec<Expr>),
}

pub fn parse_expr(s: &str) -> Result<Expr> {
    let mut parser = Parser::new(s);
    parser.expr()
}

pub fn parse_root(s: &str) -> Result<Vec<Expr>> {
    let mut parser = Parser::new(s);
    parser.root()
}

pub struct Parser<'a> {
    input: &'a str,
    pos: TextPos,
}

impl Parser<'_> {
    pub fn new(input: &str) -> Parser<'_> {
        Parser {
            input,
            pos: TextPos {
                line: 1,
                col: 1,
                byte: 0,
            },
        }
    }

    fn remaining(&self) -> &str {
        &self.input[self.pos.byte..]
    }

    fn peek(&self) -> Option<char> {
        self.remaining().chars().next()
    }

    fn peek_word(&self) -> Option<&str> {
        let rem = self.remaining();
        let len = rem
            .chars()
            .take_while(|&c| !c.is_whitespace() && c != ')')
            .map(|c| c.len_utf8())
            .sum();
        Some(&rem[..len]).filter(|s| !s.is_empty())
    }

    fn advance(&mut self, bytes: usize) -> TextRange {
        let start = self.pos;

        for c in self.input[self.pos.byte..self.pos.byte + bytes].chars() {
            if c == '\n' {
                self.pos.line += 1;
                self.pos.col = 1;
            } else {
                self.pos.col += 1;
            }
        }

        self.pos.byte += bytes;

        let end = self.pos;
        TextRange::new(start, end)
    }

    fn skip_trivia(&mut self) {
        loop {
            match self.peek() {
                Some(c) if c.is_whitespace() => {
                    self.advance(1);
                }
                Some(';') => {
                    self.advance(1);
                    while self.peek().is_some_and(|v| v != '\n') {
                        self.advance(1);
                    }
                    self.advance(1);
                }
                _ => break,
            }
        }
    }

    fn error(&self, message: &str) -> Error {
        anyhow!("{}: syntax error: {message}", self.pos)
    }

    pub fn root(&mut self) -> Result<Vec<Expr>> {
        let mut list = Vec::new();
        loop {
            list.push(self.expr()?);
            self.skip_trivia();
            if self.peek().is_none() {
                break Ok(list);
            }
        }
    }

    pub fn expr(&mut self) -> Result<Expr> {
        self.skip_trivia();

        match self.peek() {
            Some(c) if c.is_ascii_digit() => self.expr_int(),
            Some('-' | '+') if self.remaining()[1..].starts_with(|c: char| c.is_ascii_digit()) => {
                self.expr_int()
            }
            Some('"') => self.expr_string(),
            Some('(') => self.expr_list(),
            Some(_) => self.expr_symbol(),
            None => Err(self.error("expression expected")),
        }
    }

    fn expr_int(&mut self) -> Result<Expr> {
        let mut word = self
            .peek_word()
            .ok_or_else(|| self.error("integer expected"))?;

        let mut is_negative = false;
        let mut prefix_len = 1;

        if let Some(suff) = word.strip_prefix('-') {
            word = suff;
            is_negative = true;
        } else if let Some(suff) = word.strip_prefix('+') {
            word = suff;
        } else {
            prefix_len = 0;
        }

        let mut res = if let Some(suff) = word.strip_prefix("0x") {
            i64::from_str_radix(suff, 16)
        } else if let Some(suff) = word.strip_prefix("0o") {
            i64::from_str_radix(suff, 8)
        } else if let Some(suff) = word.strip_prefix("0b") {
            i64::from_str_radix(suff, 2)
        } else {
            word.parse::<i64>()
        };

        if is_negative {
            res = res.map(|v| -v);
        }

        let kind = res
            .map(ExprKind::Int)
            .map_err(|_| self.error("invalid integer"))?;

        let len = prefix_len + word.len();
        let range = self.advance(len);

        Ok(Expr { kind, range })
    }

    fn expr_string(&mut self) -> Result<Expr> {
        let rem = self.remaining();
        let mut len = 0;

        if !rem.starts_with('"') {
            return Err(self.error("string expected"));
        }

        len += 1;

        while let Some(idx) = rem[len..].find('"') {
            len += idx + 1;
            if rem[..len - 1].ends_with('\\') {
                continue;
            }

            let str = rem[1..len - 1]
                .replace("\\\\", "\\")
                .replace("\\\"", "\"")
                .replace("\\r", "\r")
                .replace("\\n", "\n")
                .replace("\\t", "\t");

            let kind = ExprKind::String(str);
            let range = self.advance(len);
            return Ok(Expr { kind, range });
        }

        self.advance(rem.len());
        Err(self.error("unterminated string"))
    }

    fn expr_symbol(&mut self) -> Result<Expr> {
        let word = self
            .peek_word()
            .ok_or_else(|| self.error("symbol expected"))?
            .to_string();

        let range = self.advance(word.len());
        let kind = ExprKind::Symbol(word);
        Ok(Expr { kind, range })
    }

    fn expr_list(&mut self) -> Result<Expr> {
        if self.peek() != Some('(') {
            return Err(self.error("list expected"));
        }

        let start = self.advance(1).start;
        let mut list = Vec::new();

        while self.peek() != Some(')') {
            list.push(self.expr()?);
        }

        let end = self.advance(1).end;
        let kind = ExprKind::List(list);
        let range = TextRange::new(start, end);
        Ok(Expr { kind, range })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn examples() -> Result<()> {
        assert_eq!(
            parse_expr("123")?,
            Expr {
                kind: ExprKind::Int(123),
                range: TextRange::new(TextPos::new(1, 1, 0), TextPos::new(1, 4, 3))
            }
        );

        assert_eq!(
            parse_expr("-0x123")?,
            Expr {
                kind: ExprKind::Int(-0x123),
                range: TextRange::new(TextPos::new(1, 1, 0), TextPos::new(1, 7, 6))
            }
        );

        assert_eq!(
            parse_expr("+0b111")?,
            Expr {
                kind: ExprKind::Int(0b111),
                range: TextRange::new(TextPos::new(1, 1, 0), TextPos::new(1, 7, 6))
            }
        );

        assert_eq!(
            parse_expr(r#""hello \"world\"""#)?,
            Expr {
                kind: ExprKind::String("hello \"world\"".into()),
                range: TextRange::new(TextPos::new(1, 1, 0), TextPos::new(1, 18, 17))
            }
        );

        assert_eq!(
            parse_expr("(+ 12 345)")?,
            Expr {
                kind: ExprKind::List(vec![
                    Expr {
                        kind: ExprKind::Symbol("+".into()),
                        range: TextRange::new(TextPos::new(1, 2, 1), TextPos::new(1, 3, 2)),
                    },
                    Expr {
                        kind: ExprKind::Int(12),
                        range: TextRange::new(TextPos::new(1, 4, 3), TextPos::new(1, 6, 5))
                    },
                    Expr {
                        kind: ExprKind::Int(345),
                        range: TextRange::new(TextPos::new(1, 7, 6), TextPos::new(1, 10, 9))
                    }
                ]),
                range: TextRange::new(TextPos::new(1, 1, 0), TextPos::new(1, 11, 10))
            }
        );

        Ok(())
    }
}
