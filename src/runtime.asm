start:
  call main.0
  hlt

bump_start:
  u32 0x80000

print:
  lw r1 <- [r0]
  add r0 <- r0, 4
  mov r3 <- 0
print.loop:
  cmpltu r2 <- r3, r1
  jz print.end, r2
  lb r2 <- [r0+r3]
  out 1 <- r2
  add r3 <- r3, 1
  jmp print.loop
print.end:
  ret

println:
  sub sp <- sp, 4
  sw [sp] <- ra
  call print
  mov r0 <- 10 ; newline
  out 1 <- r0
  lw ra <- [sp]
  add sp <- sp, 4
  ret

readln:
  lw r0 <- bump_start
  mov r1 <- 4
readln.loop:
  in r2 <- 0
  cmpeq r3 <- r2, -1  ; eof
  jnz readln.end, r3
  cmpeq r3 <- r2, 10  ; newline
  jnz readln.end, r3
  sb [r0+r1] <- r2
  add r1 <- r1, 1
  jmp readln.loop
readln.end:
  sub r2 <- r1, 4
  sw [r0] <- r2
  add r1 <- r0, r1
  sw bump_start <- r1
  ret

read_all:
  lw r0 <- bump_start
  mov r1 <- 4
read_all.loop:
  in r2 <- 0
  cmpeq r3 <- r2, -1  ; eof
  jnz read_all.end, r3
  sb [r0+r1] <- r2
  add r1 <- r1, 1
  jmp read_all.loop
read_all.end:
  sub r2 <- r1, 4
  sw [r0] <- r2
  add r1 <- r0, r1
  sw bump_start <- r1
  ret

i32_to_str:
  lw r1 <- bump_start
  add r2 <- r1, 15
  sw bump_start <- r2
  mov r3 <- 0
  cmplts r2 <- r0, 0
  jz i32_to_str.l0, r2
  mov r3 <- 0x2d ; minus
  neg r0 <- r0
i32_to_str.l0:
  mov r2 <- 14
i32_to_str.l1:
  remu r4 <- r0, 10
  add r4 <- r4, 0x30 ; '0'
  sb [r1+r2] <- r4
  sub r2 <- r2, 1
  divu r0 <- r0, 10
  jnz i32_to_str.l1, r0
  add r2 <- r2, 1
  jz i32_to_str.l2, r3
  sub r2 <- r2, 1
  sb [r1+r2] <- r3
i32_to_str.l2:
  sub r3 <- r2, 15
  neg r3 <- r3
  sub r2 <- r2, 4
  sw [r1+r2] <- r3
  add r0 <- r1, r2
  ret
