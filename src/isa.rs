use std::fmt::{self, Display};

use strum::{EnumIter, FromRepr, IntoStaticStr};

#[derive(Debug, Clone, Copy, Eq, PartialEq, Default)]
pub struct Instr {
    pub has_imm: bool,
    pub opcode: Opcode,
    pub rd: Reg,
    pub rs1: Reg,
    pub rs2: Reg,
    pub imm: Imm,
}

impl Instr {
    pub fn decode(word: u32) -> Option<Instr> {
        let has_imm = word & 1 != 0;
        let opcode = Opcode::decode(((word >> 1) & 0x7F) as u8)?;
        let rd = Reg::decode(((word >> 8) & 0xF) as u8)?;
        let rs1 = Reg::decode(((word >> 12) & 0xF) as u8)?;
        let (rs2, imm) = if has_imm {
            (Reg::R0, Imm((word >> 16) as i16))
        } else {
            if word >> 20 != 0 {
                return None;
            }
            (Reg::decode(((word >> 16) & 0xF) as u8)?, Imm(0))
        };
        Some(Instr {
            has_imm,
            opcode,
            rd,
            rs1,
            rs2,
            imm,
        })
    }

    pub fn encode(self) -> u32 {
        let word = (self.has_imm as u32)
            | (self.opcode.encode() as u32) << 1
            | (self.rd.encode() as u32) << 8
            | (self.rs1.encode() as u32) << 12;
        if self.has_imm {
            word | (self.imm.0 as u32) << 16
        } else {
            word | (self.rs2.encode() as u32) << 16
        }
    }
}

impl Display for Instr {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let Instr {
            has_imm,
            opcode,
            rd,
            rs1,
            rs2,
            imm,
        } = *self;

        let op2 = if has_imm {
            self.imm.to_string()
        } else {
            self.rs2.to_string()
        };

        match opcode {
            Opcode::General(GeneralOp::Nop) => write!(f, "nop"),
            Opcode::General(GeneralOp::Hlt) => write!(f, "hlt"),
            Opcode::General(GeneralOp::Call) if has_imm && imm.0 >= 0 => {
                write!(f, "call {rd} <- ip+{op2}")
            }
            Opcode::General(GeneralOp::Call) if has_imm => write!(f, "call {rd} <- ip{op2}"),
            Opcode::General(op) => write!(f, "{op} {rd} <- {op2}"),
            Opcode::Mem(op) if op.is_store && has_imm && imm.0 >= 0 => {
                write!(f, "{op} [{rs1}+{op2}] <- {rd}")
            }
            Opcode::Mem(op) if op.is_store && has_imm => {
                write!(f, "{op} [{rs1}{op2}] <- {rd}")
            }
            Opcode::Mem(op) if op.is_store => write!(f, "{op} [{rs1}+{op2}] <- {rd}"),
            Opcode::Mem(op) if has_imm && imm.0 >= 0 => write!(f, "{op} {rd} <- [{rs1}+{imm}]"),
            Opcode::Mem(op) if has_imm => write!(f, "{op} {rd} <- [{rs1}{op2}]"),
            Opcode::Mem(op) => write!(f, "{op} {rd} <- [{rs1}+{op2}]"),
            Opcode::Alu(op) => write!(f, "{op} {rd} <- {rs1}, {op2}"),
            Opcode::Cmp(op) => write!(f, "cmp{op} {rd} <- {rs1}, {op2}"),
            Opcode::Cond(op) => write!(f, "{op} {rd} <- {op2}, {rs1}"),
            Opcode::Io(IoOp::In) => write!(f, "in {rd} <- {imm}"),
            Opcode::Io(IoOp::Out) => write!(f, "out {imm} <- {rs2}"),
        }
    }
}

#[derive(
    Debug,
    Clone,
    Copy,
    Eq,
    PartialEq,
    Ord,
    PartialOrd,
    Hash,
    Default,
    FromRepr,
    IntoStaticStr,
    EnumIter,
)]
#[repr(u8)]
pub enum Reg {
    #[default]
    R0,
    R1,
    R2,
    R3,
    R4,
    R5,
    R6,
    R7,
    R8,
    R9,
    R10,
    R11,
    R12,
    Ra,
    Ip,
    Sp,
}

impl Reg {
    pub fn decode(byte: u8) -> Option<Reg> {
        Self::from_repr(byte)
    }

    pub fn encode(self) -> u8 {
        self as u8
    }

    pub const GENERAL_PURPOSE: &'static [Reg] = &[
        Reg::R0,
        Reg::R1,
        Reg::R2,
        Reg::R3,
        Reg::R4,
        Reg::R5,
        Reg::R6,
        Reg::R7,
        Reg::R8,
        Reg::R9,
        Reg::R10,
        Reg::R11,
        Reg::R12,
    ];

    pub const CALL_CLOBBERED: &'static [Reg] =
        &[Reg::R0, Reg::R1, Reg::R2, Reg::R3, Reg::R4, Reg::R5];

    pub const CALL_PRESERVED: &'static [Reg] = &[
        Reg::R6,
        Reg::R7,
        Reg::R8,
        Reg::R9,
        Reg::R10,
        Reg::R11,
        Reg::R12,
    ];

    pub fn is_call_preserved(self) -> bool {
        Self::CALL_PRESERVED.contains(&self)
    }

    pub fn is_call_clobbered(self) -> bool {
        Self::CALL_CLOBBERED.contains(&self)
    }
}

impl Display for Reg {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(&<&str>::from(self).to_lowercase())
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq, Default)]
pub struct Imm(pub i16);

impl Display for Imm {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum Opcode {
    General(GeneralOp),
    Mem(MemOp),
    Alu(AluOp),
    Cmp(CmpOp),
    Cond(CondOp),
    Io(IoOp),
}

impl Opcode {
    pub fn decode(byte: u8) -> Option<Opcode> {
        let kind = byte >> 4;
        let op = byte & 0b1111;
        Some(match kind {
            0 => Opcode::General(GeneralOp::decode(op)?),
            1 => Opcode::Mem(MemOp::decode(op)?),
            2 => Opcode::Alu(AluOp::decode(op)?),
            3 => Opcode::Cmp(CmpOp::decode(op)?),
            4 => Opcode::Cond(CondOp::decode(op)?),
            5 => Opcode::Io(IoOp::decode(op)?),
            _ => return None,
        })
    }

    pub fn encode(self) -> u8 {
        match self {
            Opcode::General(op) => op.encode(),
            Opcode::Mem(op) => (1 << 4) | op.encode(),
            Opcode::Alu(op) => (2 << 4) | op.encode(),
            Opcode::Cmp(op) => (3 << 4) | op.encode(),
            Opcode::Cond(op) => (4 << 4) | op.encode(),
            Opcode::Io(op) => (5 << 4) | op.encode(),
        }
    }
}

impl From<GeneralOp> for Opcode {
    fn from(op: GeneralOp) -> Self {
        Opcode::General(op)
    }
}

impl From<MemOp> for Opcode {
    fn from(op: MemOp) -> Self {
        Opcode::Mem(op)
    }
}

impl From<AluOp> for Opcode {
    fn from(op: AluOp) -> Self {
        Opcode::Alu(op)
    }
}

impl From<CmpOp> for Opcode {
    fn from(op: CmpOp) -> Self {
        Opcode::Cmp(op)
    }
}

impl From<CondOp> for Opcode {
    fn from(op: CondOp) -> Self {
        Opcode::Cond(op)
    }
}

impl From<IoOp> for Opcode {
    fn from(op: IoOp) -> Self {
        Opcode::Io(op)
    }
}

impl Default for Opcode {
    fn default() -> Opcode {
        Opcode::General(GeneralOp::default())
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq, Default, FromRepr, IntoStaticStr)]
#[repr(u8)]
pub enum GeneralOp {
    #[default]
    Nop,
    Hlt,
    Mov,
    Not,
    Neg,
    Call,
}

impl GeneralOp {
    pub fn decode(byte: u8) -> Option<GeneralOp> {
        Self::from_repr(byte)
    }

    pub fn encode(self) -> u8 {
        self as u8
    }
}

impl Display for GeneralOp {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(&<&str>::from(self).to_lowercase())
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq, Default)]
pub struct MemOp {
    pub is_store: bool,
    pub size: MemSize,
}

impl MemOp {
    pub fn decode(byte: u8) -> Option<MemOp> {
        if byte >> 3 != 0 {
            return None;
        }
        let is_store = byte >> 2 != 0;
        let size = MemSize::from_repr(byte & 0b11)?;
        Some(MemOp { is_store, size })
    }

    pub fn encode(self) -> u8 {
        (self.is_store as u8) << 2 | (self.size as u8)
    }
}

impl Display for MemOp {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(match (self.is_store, self.size) {
            (true, MemSize::Byte) => "sb",
            (true, MemSize::Half) => "sh",
            (true, MemSize::Word) => "sw",
            (false, MemSize::Byte) => "lb",
            (false, MemSize::Half) => "lh",
            (false, MemSize::Word) => "lw",
        })
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq, Default, FromRepr)]
#[repr(u8)]
pub enum MemSize {
    #[default]
    Byte,
    Half,
    Word,
}

#[derive(Debug, Clone, Copy, Eq, PartialEq, Default, FromRepr, IntoStaticStr)]
#[repr(u8)]
pub enum AluOp {
    #[default]
    Add,
    Sub,
    And,
    Or,
    Xor,
    Shl,
    Shrl,
    Shra,
    Muls,
    Divs,
    Rems,
    Mulu,
    Divu,
    Remu,
}

impl AluOp {
    pub fn decode(byte: u8) -> Option<AluOp> {
        Self::from_repr(byte)
    }

    pub fn encode(self) -> u8 {
        self as u8
    }
}

impl Display for AluOp {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(&<&str>::from(self).to_lowercase())
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq, Default, FromRepr, IntoStaticStr)]
#[repr(u8)]
pub enum CmpOp {
    #[default]
    Eq,
    Ne,
    Gts,
    Lts,
    Ges,
    Les,
    Gtu,
    Ltu,
    Geu,
    Leu,
}

impl CmpOp {
    pub fn decode(byte: u8) -> Option<CmpOp> {
        Self::from_repr(byte)
    }

    pub fn encode(self) -> u8 {
        self as u8
    }
}

impl Display for CmpOp {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(&<&str>::from(self).to_lowercase())
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq, Default)]
pub struct CondOp {
    pub is_zero: bool,
    pub kind: CondOpKind,
}

impl CondOp {
    pub fn decode(byte: u8) -> Option<CondOp> {
        if byte >> 2 != 0 {
            return None;
        }
        let is_zero = byte >> 1 != 0;
        let kind = CondOpKind::from_repr(byte & 0b1)?;
        Some(CondOp { is_zero, kind })
    }

    pub fn encode(self) -> u8 {
        (self.is_zero as u8) << 1 | (self.kind as u8)
    }
}

impl Display for CondOp {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(match (self.is_zero, self.kind) {
            (true, CondOpKind::Mov) => "movz",
            (false, CondOpKind::Mov) => "movnz",
            (true, CondOpKind::Adda) => "addaz",
            (false, CondOpKind::Adda) => "addanz",
        })
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq, Default, FromRepr)]
#[repr(u8)]
pub enum CondOpKind {
    #[default]
    Mov,
    Adda,
}

#[derive(Debug, Clone, Copy, Eq, PartialEq, Default, FromRepr)]
#[repr(u8)]
pub enum IoOp {
    #[default]
    In,
    Out,
}

impl IoOp {
    pub fn decode(byte: u8) -> Option<IoOp> {
        Self::from_repr(byte)
    }

    pub fn encode(self) -> u8 {
        self as u8
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn instr_encoding_examples() {
        let examples = [
            (
                // add r0, r1, 0x1234
                Instr {
                    has_imm: true,
                    opcode: Opcode::Alu(AluOp::Add),
                    rd: Reg::R0,
                    rs1: Reg::R1,
                    rs2: Reg::R0,
                    imm: Imm(0x1234),
                },
                0x12341041,
            ),
            (
                // mov r0, r1
                Instr {
                    has_imm: false,
                    opcode: Opcode::General(GeneralOp::Mov),
                    rd: Reg::R0,
                    rs1: Reg::R0,
                    rs2: Reg::R1,
                    imm: Imm(0),
                },
                0x00010004,
            ),
        ];

        for (instr, encoding) in examples {
            assert_eq!(instr.encode(), encoding);
            assert_eq!(Instr::decode(encoding), Some(instr));
        }
    }

    #[test]
    fn opcode_encoding_examples() {
        let examples = [
            (Opcode::General(GeneralOp::Nop), 0b000_0000),
            (Opcode::General(GeneralOp::Hlt), 0b000_0001),
            (Opcode::Alu(AluOp::Sub), 0b010_0001),
            (Opcode::Cmp(CmpOp::Ne), 0b011_0001),
            (Opcode::Io(IoOp::In), 0b101_0000),
            (
                Opcode::Mem(MemOp {
                    is_store: true,
                    size: MemSize::Half,
                }),
                0b001_0101,
            ),
            (
                Opcode::Cond(CondOp {
                    is_zero: false,
                    kind: CondOpKind::Adda,
                }),
                0b100_0001,
            ),
        ];

        for (opcode, encoding) in examples {
            assert_eq!(Opcode::decode(encoding), Some(opcode));
            assert_eq!(opcode.encode(), encoding);
        }
    }

    #[test]
    fn opcode_encoding_round_trip() {
        for encoding in 0..128 {
            let Some(opcode) = Opcode::decode(encoding) else {
                continue;
            };
            assert_eq!(opcode.encode(), encoding);
        }
    }
}
