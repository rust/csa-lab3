use std::fs::File;
use std::io::{BufReader, BufWriter, Read, Write};
use std::path::{Path, PathBuf};

use anyhow::{Context, Result};
use csa_lab3::asm::Assembler;
use csa_lab3::ir::{lower, reg_alloc, simplify};
use csa_lab3::runtime::*;
use csa_lab3::syntax::parse_root;

fn main() -> Result<()> {
    let command = <Command as clap::Parser>::try_parse()?;
    match command {
        Command::Run(cmd) => cmd_run(cmd),
        Command::Asm(cmd) => cmd_asm(cmd),
        Command::Ir(cmd) => cmd_ir(cmd),
        Command::Compile(cmd) => cmd_compile(cmd),
    }
}

#[derive(clap::Parser)]
enum Command {
    Run(CmdRun),
    Asm(CmdAsm),
    Ir(CmdIr),
    Compile(CmdCompile),
}

/// Translate source code to machine code and run it
#[derive(clap::Args)]
struct CmdRun {
    /// Input file
    input: PathBuf,
    /// Program stdin
    #[arg(long)]
    stdin: Option<PathBuf>,
    /// Program stdout
    #[arg(long)]
    stdout: Option<PathBuf>,
    /// Log path
    #[arg(short, long)]
    log_output: Option<PathBuf>,
    /// Assembly dump path
    #[arg(short, long)]
    asm_output: Option<PathBuf>,
}

/// Translate assembly to machine code
#[derive(clap::Args)]
struct CmdAsm {
    /// Input file
    input: PathBuf,
    /// Output file (default - stdout)
    #[arg(short, long)]
    output: Option<PathBuf>,
}

/// Translate source code to IR
#[derive(clap::Args)]
struct CmdIr {
    /// Input file
    input: PathBuf,
    /// Output file (default - stdout)
    #[arg(short, long)]
    output: Option<PathBuf>,
}

/// Translate source code to machine code
#[derive(clap::Args)]
struct CmdCompile {
    /// Input file
    input: PathBuf,
    /// Output file (default - stdout)
    #[arg(short, long)]
    output: Option<PathBuf>,
}

fn cmd_run(cmd: CmdRun) -> Result<()> {
    let input = get_input_file(&cmd.input)?;
    let asm_output = match &cmd.asm_output {
        Some(v) => Some(get_output_file(v)?),
        _ => None,
    };

    let log_output = match &cmd.log_output {
        Some(v) => Some(get_output_file(v)?),
        _ => None,
    };

    let stdin = get_stdin_or_file(cmd.stdin.as_deref())?;
    let stdout = get_stdout_or_file(cmd.stdout.as_deref())?;

    let runtime = compile_runtime()?;
    let code = compile_program(input, asm_output)?;
    let memory = link_program(runtime, code)?;

    run_machine(memory, stdin, stdout, log_output)?;

    Ok(())
}

fn cmd_asm(cmd: CmdAsm) -> Result<()> {
    let mut input = get_input_file(&cmd.input)?;
    let mut output = get_stdout_or_file(cmd.output.as_deref())?;

    let mut asm = Assembler::new(0);
    asm.parse_reader(&mut input)?;

    for line in asm.generate_dump() {
        writeln!(output, "{line}")?;
    }
    output.flush()?;

    Ok(())
}

fn cmd_ir(cmd: CmdIr) -> Result<()> {
    let mut input = get_input_file(&cmd.input)?;
    let mut output = get_stdout_or_file(cmd.output.as_deref())?;

    let mut source = String::new();
    input.read_to_string(&mut source)?;

    let root = parse_root(&source)?;
    let mut ir = lower(&root, &builtins())?;
    simplify(&mut ir);
    reg_alloc(&mut ir);

    write!(output, "{}", ir.stringify())?;
    output.flush()?;

    Ok(())
}

fn cmd_compile(cmd: CmdCompile) -> Result<()> {
    let input = get_input_file(&cmd.input)?;
    let output = get_stdout_or_file(cmd.output.as_deref())?;

    let runtime = compile_runtime()?;
    let code = compile_program(input, Some(output))?;
    let _memory = link_program(runtime, code)?;

    Ok(())
}

fn get_input_file(path: &Path) -> Result<BufReader<File>> {
    File::open(path)
        .map(BufReader::new)
        .with_context(|| format!("failed to open {}", path.display()))
}

fn get_output_file(path: &Path) -> Result<BufWriter<File>> {
    File::create(path)
        .map(BufWriter::new)
        .with_context(|| format!("failed to open {}", path.display()))
}

fn get_stdin_or_file(path: Option<&Path>) -> Result<Box<dyn Read>> {
    match path {
        Some(path) => Ok(Box::new(get_input_file(path)?)),
        None => Ok(Box::new(std::io::stdin())),
    }
}

fn get_stdout_or_file(path: Option<&Path>) -> Result<Box<dyn Write>> {
    match path {
        Some(path) => Ok(Box::new(get_output_file(path)?)),
        None => Ok(Box::new(std::io::stdout())),
    }
}
