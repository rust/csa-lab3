use std::collections::HashMap;

use anyhow::{anyhow, bail, Result};

use crate::asm::*;
use crate::ir::*;
use crate::isa::*;

pub fn codegen(asm: &mut Assembler, ir: &IrRoot) -> Result<()> {
    let mut ctx = CodegenContext::new(asm, ir);
    ctx.gen()?;
    Ok(())
}

#[derive(Debug)]
struct CodegenContext<'a> {
    asm: &'a mut Assembler,
    ir: &'a IrRoot,

    const_labels: HashMap<ConstId, LabelId>,
    func_labels: HashMap<FuncId, LabelId>,
    bb_labels: HashMap<BasicBlockId, LabelId>,

    cur_func_id: FuncId,
    cur_func_stack_space: i16,
    cur_func_slots_offset: i16,
    cur_func_has_calls: bool,
    cur_func_preserve_regs: Vec<Reg>,
    cur_func_clobber_regs: Vec<Reg>,
    cur_bb_id: BasicBlockId,
}

impl CodegenContext<'_> {
    fn new<'a>(asm: &'a mut Assembler, ir: &'a IrRoot) -> CodegenContext<'a> {
        CodegenContext {
            asm,
            ir,
            const_labels: Default::default(),
            func_labels: Default::default(),
            bb_labels: Default::default(),
            cur_func_id: Default::default(),
            cur_func_stack_space: Default::default(),
            cur_func_slots_offset: Default::default(),
            cur_func_has_calls: Default::default(),
            cur_func_preserve_regs: Default::default(),
            cur_func_clobber_regs: Default::default(),
            cur_bb_id: Default::default(),
        }
    }

    fn gen(&mut self) -> Result<()> {
        if self.gen_consts()? {
            self.asm.newline();
        }

        for (id, func) in self.ir.funcs.iter().enumerate() {
            let name = &format!("{}.{}", func.signature.name, id);
            self.func_labels
                .insert(FuncId(id), self.asm.get_label(name));
        }

        for id in 0..self.ir.funcs.len() {
            self.gen_func(FuncId(id))?;
        }

        Ok(())
    }

    fn gen_consts(&mut self) -> Result<bool> {
        let mut had_any = false;

        for (id, value) in self.ir.consts.iter() {
            let is_imm = match *self.ir.consts.get(id) {
                Const::Bool(_) => true,
                Const::I32(value) => i16::try_from(value).is_ok(),
                _ => false,
            };

            if is_imm {
                continue;
            }

            let label = self.asm.get_label(&format!("const_{}", id.0));
            self.asm.set_label(label)?;
            self.const_labels.insert(id, label);

            match value {
                Const::I32(val) => self.asm.i32(*val),
                Const::String(val) => self.asm.string(val),
                _ => {}
            }

            had_any = true;
        }

        Ok(had_any)
    }

    fn gen_func(&mut self, id: FuncId) -> Result<()> {
        self.cur_func_id = id;

        let cur_func = &self.ir.funcs[id.0];
        self.cur_func_stack_space = (cur_func.used_slots as i16) * 4;
        self.cur_func_slots_offset = 0;
        self.cur_func_has_calls = false;

        'outer: for bb in &cur_func.basic_blocks {
            for instr in &bb.instrs {
                if let IrInstr::Call { .. } | IrInstr::CallBuiltin { .. } = instr {
                    self.cur_func_has_calls = true;
                    break 'outer;
                }
            }
        }

        if self.cur_func_has_calls {
            self.cur_func_slots_offset += 4;
        }

        for &reg in Reg::CALL_PRESERVED {
            if cur_func.vregs.iter().any(|v| v.reg == Some(reg)) {
                self.cur_func_preserve_regs.push(reg);
                self.cur_func_slots_offset += 4;
            }
        }

        for &reg in Reg::CALL_CLOBBERED {
            if cur_func.vregs.iter().any(|v| v.reg == Some(reg)) {
                self.cur_func_clobber_regs.push(reg);
            }
        }

        self.cur_func_stack_space += self.cur_func_slots_offset;

        self.gen_func_labels();
        self.gen_func_prologue()?;
        self.gen_func_body()?;
        self.asm.newline();

        Ok(())
    }

    fn gen_func_labels(&mut self) {
        let cur_func = &self.ir.funcs[self.cur_func_id.0];

        self.bb_labels.clear();
        for id in 0..cur_func.basic_blocks.len() {
            let name = &format!(
                "{}.{}.bb{}",
                cur_func.signature.name, self.cur_func_id.0, id
            );

            self.bb_labels
                .insert(BasicBlockId(id), self.asm.get_label(name));
        }
    }

    fn gen_func_prologue(&mut self) -> Result<()> {
        self.asm.set_label(self.func_labels[&self.cur_func_id])?;

        if self.cur_func_stack_space > 0 {
            self.asm.instr(AsmInstr::Sub {
                dst: Reg::Sp,
                src1: Reg::Sp,
                src2: Imm(self.cur_func_stack_space).into(),
            })?;
        }

        if self.cur_func_has_calls {
            self.asm.instr(AsmInstr::Sw {
                dst: MemAddr::Rel(Reg::Sp, Imm(0).into()),
                src: Reg::Ra,
            })?;
        }

        for (i, &reg) in self.cur_func_preserve_regs.iter().enumerate() {
            let offset = if self.cur_func_has_calls { 4 } else { 0 };
            self.asm.instr(AsmInstr::Sw {
                dst: MemAddr::Rel(Reg::Sp, Imm((i as i16) * 4 + offset).into()),
                src: reg,
            })?;
        }

        Ok(())
    }

    fn gen_func_epilogue(&mut self) -> Result<()> {
        if self.cur_func_has_calls {
            self.asm.instr(AsmInstr::Lw {
                dst: Reg::Ra,
                src: MemAddr::Rel(Reg::Sp, Imm(0).into()),
            })?;
        }

        for (i, &reg) in self.cur_func_preserve_regs.iter().enumerate() {
            let offset = if self.cur_func_has_calls { 4 } else { 0 };
            self.asm.instr(AsmInstr::Lw {
                dst: reg,
                src: MemAddr::Rel(Reg::Sp, Imm((i as i16) * 4 + offset).into()),
            })?;
        }

        if self.cur_func_stack_space > 0 {
            self.asm.instr(AsmInstr::Add {
                dst: Reg::Sp,
                src1: Reg::Sp,
                src2: Imm(self.cur_func_stack_space).into(),
            })?;
        }

        self.asm.instr(AsmInstr::Ret)?;

        Ok(())
    }

    fn gen_func_body(&mut self) -> Result<()> {
        let cur_func = &self.ir.funcs[self.cur_func_id.0];
        for (id, bb) in cur_func.basic_blocks.iter().enumerate() {
            self.cur_bb_id = BasicBlockId(id);

            let label = self.bb_labels[&self.cur_bb_id];
            self.asm.set_label(label)?;

            for instr in &bb.instrs {
                self.gen_ir_instr(instr)?;
            }

            self.gen_terminator(&bb.terminator)?;
        }

        Ok(())
    }

    fn gen_ir_instr(&mut self, instr: &IrInstr) -> Result<()> {
        match *instr {
            IrInstr::Copy { dst, src } => {
                let dst = self.reg(dst)?;
                let src = self.reg(src)?;
                if dst != src {
                    let src = src.into();
                    self.asm.instr(AsmInstr::Mov { dst, src })?;
                }
            }

            IrInstr::UnOp { op, dst, src } => {
                let dst = self.reg(dst)?;
                let src = self.reg(src)?;
                self.asm.instr(match op {
                    UnOp::Not => AsmInstr::Not { dst, src },
                    UnOp::Neg => AsmInstr::Neg { dst, src },
                })?;
            }

            IrInstr::BinOp {
                op,
                dst,
                src1,
                src2,
            } => {
                let dst = self.reg(dst)?;
                let src1 = self.reg(src1)?;
                let src2 = self.reg(src2)?.into();

                self.asm.instr(match op {
                    BinOp::Add => AsmInstr::Add { dst, src1, src2 },
                    BinOp::Sub => AsmInstr::Sub { dst, src1, src2 },
                    BinOp::And => AsmInstr::And { dst, src1, src2 },
                    BinOp::Or => AsmInstr::Or { dst, src1, src2 },
                    BinOp::Xor => AsmInstr::Xor { dst, src1, src2 },
                    BinOp::Shl => AsmInstr::Shl { dst, src1, src2 },
                    BinOp::Shr => AsmInstr::Shra { dst, src1, src2 },
                    BinOp::Mul => AsmInstr::Muls { dst, src1, src2 },
                    BinOp::Div => AsmInstr::Divs { dst, src1, src2 },
                    BinOp::Rem => AsmInstr::Rems { dst, src1, src2 },
                    BinOp::CmpEq => AsmInstr::CmpEq { dst, src1, src2 },
                    BinOp::CmpNe => AsmInstr::CmpNe { dst, src1, src2 },
                    BinOp::CmpGt => AsmInstr::CmpGts { dst, src1, src2 },
                    BinOp::CmpLt => AsmInstr::CmpLts { dst, src1, src2 },
                    BinOp::CmpGe => AsmInstr::CmpGes { dst, src1, src2 },
                    BinOp::CmpLe => AsmInstr::CmpLes { dst, src1, src2 },
                })?;
            }

            IrInstr::LoadArg { .. } => {
                // do nothing
            }

            IrInstr::LoadConst { dst, id } => {
                let dst = self.reg(dst)?;

                let (imm, is_lea) = match *self.ir.consts.get(id) {
                    Const::Bool(value) => (Some(value as i16), false),
                    Const::I32(value) => (i16::try_from(value).ok(), false),
                    Const::String(_) => (None, true),
                };

                let instr = match imm.map(Imm) {
                    Some(v) => AsmInstr::Mov { dst, src: v.into() },
                    None if is_lea => AsmInstr::Lea {
                        dst,
                        src: MemAddr::Label(self.const_labels[&id]),
                    },
                    None => AsmInstr::Lw {
                        dst,
                        src: MemAddr::Label(self.const_labels[&id]),
                    },
                };

                self.asm.instr(instr)?;
            }

            IrInstr::LoadVar { dst, slot } => {
                let dst = self.reg(dst)?;
                let src = MemAddr::Rel(
                    Reg::Sp,
                    Imm(self.cur_func_slots_offset + 4 * (slot as i16)).into(),
                );
                self.asm.instr(AsmInstr::Lw { dst, src })?;
            }

            IrInstr::StoreVar { src, slot } => {
                let src = self.reg(src)?;
                let dst = MemAddr::Rel(
                    Reg::Sp,
                    Imm(self.cur_func_slots_offset + 4 * (slot as i16)).into(),
                );
                self.asm.instr(AsmInstr::Sw { src, dst })?;
            }

            IrInstr::Call { func, .. } => {
                let label = self.func_labels[&func];
                let return_ty = self.ir.funcs[func.0].signature.ret;
                self.gen_call(label, return_ty)?;
            }

            IrInstr::CallBuiltin { ref name, .. } => {
                let label = self.asm.get_label(name);
                let return_ty = self.ir.builtins.get(name).unwrap().ret;
                self.gen_call(label, return_ty)?;
            }
        }

        Ok(())
    }

    fn gen_call(&mut self, label: LabelId, return_ty: Type) -> Result<()> {
        let has_return = !return_ty.is(Type::Unit);

        let mut num_saved = self.cur_func_clobber_regs.len() as i16;
        if has_return && self.cur_func_clobber_regs.first() == Some(&Reg::R0) {
            num_saved -= 1;
        }

        if num_saved > 0 {
            let mut imm = Imm(4 * num_saved);

            self.asm.instr(AsmInstr::Sub {
                dst: Reg::Sp,
                src1: Reg::Sp,
                src2: imm.into(),
            })?;

            for &reg in &self.cur_func_clobber_regs {
                if has_return && reg == Reg::R0 {
                    continue;
                }

                imm.0 -= 4;
                self.asm.instr(AsmInstr::Sw {
                    dst: MemAddr::Rel(Reg::Sp, imm.into()),
                    src: reg,
                })?;
            }
        }

        let addr = JmpAddr::Label(label);
        self.asm.instr(AsmInstr::Call { addr })?;

        if num_saved > 0 {
            let mut imm = Imm(4 * num_saved);
            for &reg in &self.cur_func_clobber_regs {
                if has_return && reg == Reg::R0 {
                    continue;
                }

                imm.0 -= 4;
                self.asm.instr(AsmInstr::Lw {
                    dst: reg,
                    src: MemAddr::Rel(Reg::Sp, imm.into()),
                })?;
            }

            self.asm.instr(AsmInstr::Add {
                dst: Reg::Sp,
                src1: Reg::Sp,
                src2: Imm(4 * num_saved).into(),
            })?;
        }

        Ok(())
    }

    fn gen_terminator(&mut self, terminator: &Terminator) -> Result<()> {
        match *terminator {
            Terminator::RetValue { .. } => {
                self.gen_func_epilogue()?;
            }

            Terminator::Jmp(dst) => {
                if dst.0 != self.cur_bb_id.0 + 1 {
                    let addr = JmpAddr::Label(self.bb_labels[&dst]);
                    self.asm.instr(AsmInstr::Jmp { addr })?;
                }
            }

            Terminator::JmpIf {
                cond,
                if_true,
                if_false,
            } => {
                let cond = self.reg(cond)?;

                if if_true.0 == self.cur_bb_id.0 + 1 {
                    let addr = JmpAddr::Label(self.bb_labels[&if_false]);
                    self.asm.instr(AsmInstr::Jz { addr, cond })?;
                } else if if_false.0 == self.cur_bb_id.0 + 1 {
                    let addr = JmpAddr::Label(self.bb_labels[&if_true]);
                    self.asm.instr(AsmInstr::Jnz { addr, cond })?;
                } else {
                    let addr = JmpAddr::Label(self.bb_labels[&if_true]);
                    self.asm.instr(AsmInstr::Jnz { addr, cond })?;

                    let addr = JmpAddr::Label(self.bb_labels[&if_false]);
                    self.asm.instr(AsmInstr::Jmp { addr })?;
                }
            }

            Terminator::Unset => bail!("invalid ir: terminator unset"),
        }

        Ok(())
    }

    fn reg(&mut self, vreg: VReg) -> Result<Reg> {
        let cur_func = &self.ir.funcs[self.cur_func_id.0];
        let info = &cur_func.vregs[vreg.0];

        if info.ty.is(Type::Unit) {
            return Ok(Reg::R0);
        }

        info.reg
            .ok_or_else(|| anyhow!("invalid ir: {vreg} is not allocated"))
    }
}
