use std::ffi::OsString;
use std::io::{Read, Write};
use std::process::{Command, Stdio};

use anyhow::{anyhow, bail, Result};
use indexmap::IndexMap;
use serde::{Deserialize, Serialize};
use tempfile::NamedTempFile;
use walkdir::WalkDir;

const TESTS_ROOT: &'static str = concat!(env!("CARGO_MANIFEST_DIR"), "/tests/golden");
const EXE_PATH: &'static str = env!("CARGO_BIN_EXE_csa-lab3");

#[derive(Serialize, Deserialize)]
struct GoldenTest {
    args: String,
    input_files: IndexMap<String, String>,
    output_files: IndexMap<String, String>,
}

fn main() -> Result<()> {
    let update = std::env::var("GOLDEN_UPDATE").is_ok_and(|v| v == "1");

    let mut all_passed = true;

    for entry in WalkDir::new(TESTS_ROOT) {
        let entry = entry?;
        let path = entry.path();
        if path.extension().and_then(|s| s.to_str()) != Some("yaml") {
            continue;
        }

        let test_name = path.strip_prefix(TESTS_ROOT)?.to_string_lossy();

        let yaml = match std::fs::read_to_string(&path) {
            Ok(v) => v,
            Err(e) => {
                println!("Failed to open {}, skipping: {e:?}", path.display());
                continue;
            }
        };

        let dots = ".".repeat(40usize.saturating_sub(test_name.len()));
        print!("TEST {test_name} {dots} ");

        let mut test = serde_yaml::from_str(&yaml)?;

        all_passed &= match run_test(&mut test, update) {
            Ok(true) => true,
            Ok(false) => {
                if update {
                    let str = serde_yaml::to_string(&test)?;
                    std::fs::write(&path, str)?;
                }

                false
            }
            Err(e) => {
                println!("ERR\n\n{e:?}\n");
                false
            }
        };
    }

    if !all_passed && !update {
        bail!("all tests must pass");
    }

    Ok(())
}

fn run_test(test: &mut GoldenTest, update: bool) -> Result<bool> {
    let input_files = test
        .input_files
        .iter()
        .map(|(name, contents)| -> Result<(String, NamedTempFile)> {
            let mut file = NamedTempFile::new()?;
            file.write_all(contents.as_bytes())?;
            Ok((name.clone(), file))
        })
        .collect::<Result<IndexMap<_, _>>>()?;

    let output_files = test
        .output_files
        .iter()
        .map(
            |(name, contents)| -> Result<(String, (String, NamedTempFile))> {
                Ok((name.clone(), (contents.clone(), NamedTempFile::new()?)))
            },
        )
        .collect::<Result<IndexMap<_, _>>>()?;

    let args = test
        .args
        .split_whitespace()
        .map(|arg| {
            if let Some(name) = arg.strip_prefix("$in_") {
                let file = input_files
                    .get(name)
                    .ok_or_else(|| anyhow!("no such input file: {name}"))?;
                return Ok(file.path().into());
            }
            if let Some(name) = arg.strip_prefix("$out_") {
                let (_, file) = output_files
                    .get(name)
                    .ok_or_else(|| anyhow!("no such input file: {name}"))?;
                return Ok(file.path().into());
            }
            Ok(OsString::from(arg))
        })
        .collect::<Result<Vec<_>>>()?;

    let child = Command::new(EXE_PATH)
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .args(args)
        .spawn()?;

    let output = child.wait_with_output()?;
    if !output.status.success() {
        println!("FAIL\n");
        println!("Process exited with a nonzero status code");
        println!("stdout:");
        std::io::stdout().write_all(&output.stdout)?;
        println!("stderr:");
        std::io::stdout().write_all(&output.stderr)?;
        println!();
        return Ok(false);
    }

    let mut has_failed = false;

    for (file_name, (expected, file)) in &output_files {
        let mut file = file.reopen()?;
        let mut actual = String::new();
        file.read_to_string(&mut actual)?;

        if expected == &actual {
            continue;
        }

        if !has_failed {
            println!("FAIL\n");
            has_failed = true;
        }

        let expected = expected.lines().map(|l| l.trim_end()).collect::<Vec<_>>();
        let actual = actual.lines().map(|l| l.trim_end()).collect::<Vec<_>>();

        let diff = difflib::unified_diff(
            &expected,
            &actual,
            &format!("{file_name} expected"),
            &format!("{file_name} actual  "),
            "1970-01-01 00:00:00",
            "1970-01-01 00:00:00",
            2,
        );

        for line in &diff {
            println!("{}", line.trim_end());
        }
    }

    if has_failed && update {
        println!();

        for (file_name, (_, file)) in output_files {
            let mut file = file.reopen()?;
            let mut actual = String::new();
            file.read_to_string(&mut actual)?;
            *test.output_files.get_mut(&*file_name).unwrap() = actual;
        }

        Ok(false)
    } else if has_failed {
        println!();
        Ok(false)
    } else {
        println!("PASS");
        Ok(true)
    }
}
